﻿// Decompiled with JetBrains decompiler
// Type: ⁫‪‌‪‏‍‬⁬‍‭⁯‪⁫⁭‭⁬‫⁯⁯⁫‏‌‮‏⁪⁪‍‪‏⁪⁬‎⁯​‮‎‫‏‫⁬‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[TypeLibType(4160)]
[Guid("F7898AF5-CAC4-4632-A2EC-DA06E5111AF2")]
[ComImport]
public interface \u206B‪‌‪‏‍‬⁬‍‭⁯‪⁫⁭‭⁬‫⁯⁯⁫‏‌‮‏⁪⁪‍‪‏⁪⁬‎⁯​‮‎‫‏‫⁬‮
{
  [DispId(1)]
  \u200D⁮⁫‍⁭‌⁬‮‮​‎‎‎⁪⁮‬‭⁬‎⁬⁬⁯‮‎⁬‮⁫⁬​‍‏⁮‮⁮‪‭​⁭‬‍‮ \u200C‫⁪‭⁭‭⁮​⁪‫⁬⁯‫‏​‌⁬⁯​‭‭‫⁫​⁫‏‏⁭⁭‎⁪⁪‏⁫‌‬⁬⁭‌⁬‮ { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }

  [DispId(2)]
  \u200B⁭‫‍⁬‪‮‌‌‬‍⁮⁭‬⁭⁬‬⁪‌⁫‏⁬⁬‍⁪‬‬‏​​⁯⁮⁫⁬⁭​‍‎⁭⁭‮ \u206D⁮‎‎​⁪‫⁬⁭‮⁪‪‌⁪​‬‌‎⁯‭‌​​‭‬⁯⁫‎‮‭⁪‫‎‏‎‭⁫⁬‌‮ { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(3)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void \u200F‪⁯⁭⁭‏‌‪‮⁫‫‏‌⁬⁭‌​⁯‫⁮‫‍‬‍‬‏​⁪‌‮⁪‍⁬‏‮‏‮⁭‭⁫‮();

  [DispId(4)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void \u206F⁮⁭‪​‍⁪​⁮‏‎‮​⁯‭‬‭‎⁬⁫‮⁬‫‎‭⁮‪‫‫⁫‫⁭‮⁫⁫‭⁭⁫⁬⁬‮(
    [MarshalAs(UnmanagedType.BStr), In] string _param1,
    [In] \u206F‎⁯‍‌⁭‌‮⁮⁫‎⁮⁫‌‏‪​‏‫⁬‏‍‮⁮⁬⁮⁮‫‎⁪‮‪‏‫‫‎​‫‌⁫‮ _param2,
    [In] int _param3,
    [MarshalAs(UnmanagedType.BStr), In] string _param4,
    [In] \u206E⁮⁪‪‮‍‮​⁪⁮⁯⁫‭⁬‎‎⁬‍‮‍⁪⁬⁪⁬⁮⁭​⁪​‮‏⁮‮‭‮‎‭‬‍‌‮ _param5,
    [MarshalAs(UnmanagedType.Struct)] out object _param6,
    [MarshalAs(UnmanagedType.Struct)] out object _param7);

  [DispId(5)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void \u206B‪‮‌‬⁭​‫⁫⁫‫⁪⁬⁬⁬‮‏‭‎‍‪⁯‌⁭‬‍⁫⁪​‭‍‪⁮‮⁭⁪‫​⁫⁬‮(
    [In] \u206F‎⁯‍‌⁭‌‮⁮⁫‎⁮⁫‌‏‪​‏‫⁬‏‍‮⁮⁬⁮⁮‫‎⁪‮‪‏‫‫‎​‫‌⁫‮ _param1,
    [MarshalAs(UnmanagedType.BStr), In] string _param2,
    [In] byte _param3,
    [MarshalAs(UnmanagedType.Struct)] out object _param4,
    [MarshalAs(UnmanagedType.Struct)] out object _param5);
}
