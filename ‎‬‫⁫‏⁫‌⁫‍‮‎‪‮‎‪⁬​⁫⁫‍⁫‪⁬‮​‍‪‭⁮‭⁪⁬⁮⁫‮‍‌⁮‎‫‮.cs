﻿// Decompiled with JetBrains decompiler
// Type: ‎‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

[DefaultEvent("ToggledChanged")]
internal class \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮ : Control
{
  private Timer \u206F⁬⁮‏⁭‏⁬​‪‬‫⁬‪‍‪‍‌⁭⁯⁪‫‮‎‪‭‮‭‭‬⁫⁫⁮⁪‪‮‪‫​‏‏‮ = new Timer()
  {
    Interval = 1
  };
  private int \u206D‍‮‏⁮‭⁮‭‭⁪‮⁪‬‍‭‪‍‮​⁬⁫‍⁯⁫‮⁫‪‭‮⁬​⁬‬‌‪⁯⁫⁯​‪‮ = 0;
  private Size \u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮ = new Size(15, 20);
  private bool \u200E‮‪‍⁬⁮​⁫⁬‪⁮⁬⁮‏​⁯⁭‏‏​‌‍​⁫⁬⁪⁭⁫‭‬‫⁫‎‪⁫‎⁮⁫‬⁭‮;
  private \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮.\u206D‭‪⁬‫⁯⁯‭‫⁬⁪⁮‎‪‪⁮‪‎‏‎​‫‫⁭⁬‬⁭‬‎‪‬‫‮‎⁬‭​⁭⁭‪‮ \u200B‍‪‬‌⁮​⁯‏​‎⁫⁫‮‫⁭‪‪​‭‭‪⁯‪‎​⁯⁫‍‫‫⁯⁬‌‎⁯‎‍⁪⁯‮;
  private Rectangle \u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮;

  public GraphicsPath \u206C⁪⁭⁮‫⁮⁭‎‏⁯‭‎‫‎‮‬⁫‌⁬⁬⁬‌⁪⁯‬⁪‏⁪‮‏‭⁬⁬‍⁪‏⁬‍⁬⁮‮(
    Rectangle _param1,
    \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮.\u200E‍‪‌​⁪⁪⁫​‮⁪⁯‮‍‌‬‏⁪⁫⁮‏⁪​⁫‭‌⁯‎‫‎⁬⁫⁮⁫‌⁪‏​​‬‮ _param2)
  {
    GraphicsPath graphicsPath = new GraphicsPath();
    if (!_param2.\u202C⁪⁯‫⁯⁭‎‮‪‭⁭⁬‏​⁪‍‏‏⁫⁫⁪‫‬‌‭‌‮‪⁫‪‍⁯⁫‫‌​⁭⁪‭‪‮)
      graphicsPath.AddLine(_param1.X, _param1.Y + _param1.Height, _param1.X, _param1.Y);
    else
      graphicsPath.AddArc(new Rectangle(_param1.X, _param1.Y, _param1.Height, _param1.Height), -270f, 180f);
    if (_param2.\u206B​​⁯‮⁭⁯⁪‭‏‮⁭⁫⁯‭⁬‫‮‭‪‎⁪‌⁪‪‍​‎⁮⁯‬‫⁮​‌⁭⁪​‍‏‮)
      graphicsPath.AddArc(new Rectangle(_param1.X + _param1.Width - _param1.Height, _param1.Y, _param1.Height, _param1.Height), -90f, 180f);
    else
      graphicsPath.AddLine(_param1.X + _param1.Width, _param1.Y, _param1.X + _param1.Width, _param1.Y + _param1.Height);
    graphicsPath.CloseAllFigures();
    return graphicsPath;
  }

  public object \u206C⁪⁭⁮‫⁮⁭‎‏⁯‭‎‫‎‮‬⁫‌⁬⁬⁬‌⁪⁯‬⁪‏⁪‮‏‭⁬⁬‍⁪‏⁬‍⁬⁮‮(
    int _param1,
    int _param2,
    int _param3,
    int _param4,
    \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮.\u200E‍‪‌​⁪⁪⁫​‮⁪⁯‮‍‌‬‏⁪⁫⁮‏⁪​⁫‭‌⁯‎‫‎⁬⁫⁮⁫‌⁪‏​​‬‮ _param5)
  {
    return (object) this.\u206C⁪⁭⁮‫⁮⁭‎‏⁯‭‎‫‎‮‬⁫‌⁬⁬⁬‌⁪⁯‬⁪‏⁪‮‏‭⁬⁬‍⁪‏⁬‍⁬⁮‮(new Rectangle(_param1, _param2, _param3, _param4), _param5);
  }

  public event \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮.\u206C⁭‏⁬‏‪‪‬⁬‬‫⁫‬⁯‮‮‫‫⁬​‪‎‭⁮​‌⁮‫‍⁭⁮⁯⁪‬⁪⁭⁫⁮‮‮‮ \u202E⁫‏‫​⁮‭⁬⁮‫⁮⁫‮⁫‫‏‎‎⁪‪​‍​⁭⁭‏‫​‏‫‏‏‬‮‮‭​​⁬‮‮;

  public bool \u200E⁪‮‏⁬‎‪‏‮‌‪‍⁮​‏⁮⁬‪⁯‍​⁭‏⁫​‏‌⁪‏⁬⁬​⁮‪⁫‪‎⁯‎⁫‮
  {
    get
    {
      return this.\u200E‮‪‍⁬⁮​⁫⁬‪⁮⁬⁮‏​⁯⁭‏‏​‌‍​⁫⁬⁪⁭⁫‭‬‫⁫‎‪⁫‎⁮⁫‬⁭‮;
    }
    set
    {
      this.\u200E‮‪‍⁬⁮​⁫⁬‪⁮⁬⁮‏​⁯⁭‏‏​‌‍​⁫⁬⁪⁭⁫‭‬‫⁫‎‪⁫‎⁮⁫‬⁭‮ = value;
      this.Invalidate();
      if (this.\u202E⁫‏‫​⁮‭⁬⁮‫⁮⁫‮⁫‫‏‎‎⁪‪​‍​⁭⁭‏‫​‏‫‏‏‬‮‮‭​​⁬‮‮ == null)
        return;
      this.\u202E⁫‏‫​⁮‭⁬⁮‫⁮⁫‮⁫‫‏‎‎⁪‪​‍​⁭⁭‏‫​‏‫‏‏‬‮‮‭​​⁬‮‮();
    }
  }

  public \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮.\u206D‭‪⁬‫⁯⁯‭‫⁬⁪⁮‎‪‪⁮‪‎‏‎​‫‫⁭⁬‬⁭‬‎‪‬‫‮‎⁬‭​⁭⁭‪‮ \u206D⁪‭‌‏​‍⁫⁭‌‎‫‬‪​⁪‬‪​‮⁭‌‎⁮​‎⁮‫⁯‏‎‏⁯‍⁮‪⁯⁬⁯⁮‮
  {
    get
    {
      return this.\u200B‍‪‬‌⁮​⁯‏​‎⁫⁫‮‫⁭‪‪​‭‭‪⁯‪‎​⁯⁫‍‫‫⁯⁬‌‎⁯‎‍⁪⁯‮;
    }
    set
    {
      this.\u200B‍‪‬‌⁮​⁯‏​‎⁫⁫‮‫⁭‪‪​‭‭‪⁯‪‎​⁯⁫‍‫‫⁯⁬‌‎⁯‎‍⁪⁯‮ = value;
      this.Invalidate();
    }
  }

  void Control.\u200E⁬⁪‎‮⁬‪⁫⁪‭‪‌⁯‫‬⁪‮​⁫⁮‫⁬⁫‪‮‬‫⁭⁯⁭‪⁬‍​‍‎‫‏⁬‎‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnResize(_param1));
    this.Width = 41;
    this.Height = 23;
  }

  void Control.\u200F⁫‪‫‎⁫⁭‏‏‭​‭‫‬‌⁪⁫⁬⁮⁪⁭‫‮⁬⁯‏⁪​​‎‪‫⁪​‪‮‏⁫‪‮(MouseEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnMouseUp(_param1));
    this.\u200E⁪‮‏⁬‎‪‏‮‌‪‍⁮​‏⁮⁬‪⁯‍​⁭‏⁫​‏‌⁪‏⁬⁬​⁮‪⁫‪‎⁯‎⁫‮ = !this.\u200E⁪‮‏⁬‎‪‏‮‌‪‍⁮​‏⁮⁬‪⁯‍​⁭‏⁫​‏‌⁪‏⁬⁬​⁮‪⁫‪‎⁯‎⁫‮;
  }

  public \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮()
  {
    this.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
    this.\u206F⁬⁮‏⁭‏⁬​‪‬‫⁬‪‍‪‍‌⁭⁯⁪‫‮‎‪‭‮‭‭‬⁫⁫⁮⁪‪‮‪‫​‏‏‮.Tick += new EventHandler(this.\u200C⁬⁬‏⁫⁪​⁯⁮‏​⁭⁫⁯‬‬⁮⁬‬⁮⁪⁪‎‪‪‮⁫​⁪‬‭‍‭⁪⁭⁯⁪‪⁯‭‮);
  }

  void Control.\u206F⁬​‮‬‮‫‮‪⁯‬‍‏⁭‪‌⁫‪‪⁮‫‮⁭‎⁯‍‎‎⁬‌⁯‬‎​‍⁯‏‪⁯‬‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnHandleCreated(_param1));
    this.\u206F⁬⁮‏⁭‏⁬​‪‬‫⁬‪‍‪‍‌⁭⁯⁪‫‮‎‪‭‮‭‭‬⁫⁫⁮⁪‪‮‪‫​‏‏‮.Start();
  }

  private void \u200C⁬⁬‏⁫⁪​⁯⁮‏​⁭⁫⁯‬‬⁮⁬‬⁮⁪⁪‎‪‪‮⁫​⁪‬‭‍‭⁪⁭⁯⁪‪⁯‭‮(object _param1, EventArgs _param2)
  {
    if (this.\u200E‮‪‍⁬⁮​⁫⁬‪⁮⁬⁮‏​⁯⁭‏‏​‌‍​⁫⁬⁪⁭⁫‭‬‫⁫‎‪⁫‎⁮⁫‬⁭‮)
    {
      if (this.\u206D‍‮‏⁮‭⁮‭‭⁪‮⁪‬‍‭‪‍‮​⁬⁫‍⁯⁫‮⁫‪‭‮⁬​⁬‬‌‪⁯⁫⁯​‪‮ >= 100)
        return;
      this.\u206D‍‮‏⁮‭⁮‭‭⁪‮⁪‬‍‭‪‍‮​⁬⁫‍⁯⁫‮⁫‪‭‮⁬​⁬‬‌‪⁯⁫⁯​‪‮ += 10;
      this.Invalidate(false);
    }
    else
    {
      if (this.\u206D‍‮‏⁮‭⁮‭‭⁪‮⁪‬‍‭‪‍‮​⁬⁫‍⁯⁫‮⁫‪‭‮⁬​⁬‬‌‪⁯⁫⁯​‪‮ <= 0)
        return;
      this.\u206D‍‮‏⁮‭⁮‭‭⁪‮⁪‬‍‭‪‍‮​⁬⁫‍⁯⁫‮⁫‪‭‮⁬​⁬‬‌‪⁯⁫⁯​‪‮ -= 10;
      this.Invalidate(false);
    }
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    Graphics graphics = _param1.Graphics;
    graphics.Clear(this.Parent.BackColor);
    LinearGradientBrush linearGradientBrush = new LinearGradientBrush(new Point(0, checked ((int) Math.Round(unchecked ((double) this.Height / 2.0 - (double) this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Height / 2.0)))), new Point(0, checked ((int) Math.Round(unchecked ((double) this.Height / 2.0 + (double) this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Height / 2.0)))), Color.FromArgb(250, 250, 250), Color.FromArgb(240, 240, 240));
    this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮ = new Rectangle(8, 10, checked (this.Width - 21), checked (this.Height - 21));
    graphics.SmoothingMode = SmoothingMode.AntiAlias;
    graphics.FillPath((Brush) linearGradientBrush, (GraphicsPath) this.\u206C⁪⁭⁮‫⁮⁭‎‏⁯‭‎‫‎‮‬⁫‌⁬⁬⁬‌⁪⁯‬⁪‏⁪‮‏‭⁬⁬‍⁪‏⁬‍⁬⁮‮(0, checked ((int) Math.Round(unchecked ((double) this.Height / 2.0 - (double) this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Height / 2.0))), checked (this.Width - 1), checked (this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Height - 5), new \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮.\u200E‍‪‌​⁪⁪⁫​‮⁪⁯‮‍‌‬‏⁪⁫⁮‏⁪​⁫‭‌⁯‎‫‎⁬⁫⁮⁫‌⁪‏​​‬‮()
    {
      \u202C⁪⁯‫⁯⁭‎‮‪‭⁭⁬‏​⁪‍‏‏⁫⁫⁪‫‬‌‭‌‮‪⁫‪‍⁯⁫‫‌​⁭⁪‭‪‮ = true,
      \u206B​​⁯‮⁭⁯⁪‭‏‮⁭⁫⁯‭⁬‫‮‭‪‎⁪‌⁪‪‍​‎⁮⁯‬‫⁮​‌⁭⁪​‍‏‮ = true
    }));
    graphics.DrawPath(new Pen(Color.FromArgb(177, 177, 176)), (GraphicsPath) this.\u206C⁪⁭⁮‫⁮⁭‎‏⁯‭‎‫‎‮‬⁫‌⁬⁬⁬‌⁪⁯‬⁪‏⁪‮‏‭⁬⁬‍⁪‏⁬‍⁬⁮‮(0, checked ((int) Math.Round(unchecked ((double) this.Height / 2.0 - (double) this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Height / 2.0))), checked (this.Width - 1), checked (this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Height - 5), new \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮.\u200E‍‪‌​⁪⁪⁫​‮⁪⁯‮‍‌‬‏⁪⁫⁮‏⁪​⁫‭‌⁯‎‫‎⁬⁫⁮⁫‌⁪‏​​‬‮()
    {
      \u202C⁪⁯‫⁯⁭‎‮‪‭⁭⁬‏​⁪‍‏‏⁫⁫⁪‫‬‌‭‌‮‪⁫‪‍⁯⁫‫‌​⁭⁪‭‪‮ = true,
      \u206B​​⁯‮⁭⁯⁪‭‏‮⁭⁫⁯‭⁬‫‮‭‪‎⁪‌⁪‪‍​‎⁮⁯‬‫⁮​‌⁭⁪​‍‏‮ = true
    }));
    linearGradientBrush.Dispose();
    switch (this.\u200B‍‪‬‌⁮​⁯‏​‎⁫⁫‮‫⁭‪‪​‭‭‪⁯‪‎​⁯⁫‍‫‫⁯⁬‌‎⁯‎‍⁪⁯‮)
    {
      case \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮.\u206D‭‪⁬‫⁯⁯‭‫⁬⁪⁮‎‪‪⁮‪‎‏‎​‫‫⁭⁬‬⁭‬‎‪‬‫‮‎⁬‭​⁭⁭‪‮.YesNo:
        if (!this.\u200E⁪‮‏⁬‎‪‏‮‌‪‍⁮​‏⁮⁬‪⁯‍​⁭‏⁫​‏‌⁪‏⁬⁬​⁮‪⁫‪‎⁯‎⁫‮)
        {
          graphics.DrawString("No", new Font("Segoe UI", 7f, FontStyle.Regular), Brushes.Gray, (float) checked (this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.X + 18), (float) this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Y, new StringFormat()
          {
            Alignment = StringAlignment.Center,
            LineAlignment = StringAlignment.Center
          });
          break;
        }
        graphics.DrawString("Yes", new Font("Segoe UI", 7f, FontStyle.Regular), Brushes.Gray, (float) checked (this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.X + 7), (float) this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Y, new StringFormat()
        {
          Alignment = StringAlignment.Center,
          LineAlignment = StringAlignment.Center
        });
        break;
      case \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮.\u206D‭‪⁬‫⁯⁯‭‫⁬⁪⁮‎‪‪⁮‪‎‏‎​‫‫⁭⁬‬⁭‬‎‪‬‫‮‎⁬‭​⁭⁭‪‮.OnOff:
        if (this.\u200E⁪‮‏⁬‎‪‏‮‌‪‍⁮​‏⁮⁬‪⁯‍​⁭‏⁫​‏‌⁪‏⁬⁬​⁮‪⁫‪‎⁯‎⁫‮)
        {
          graphics.DrawString("On", new Font("Segoe UI", 7f, FontStyle.Regular), Brushes.Gray, (float) checked (this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.X + 7), (float) this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Y, new StringFormat()
          {
            Alignment = StringAlignment.Center,
            LineAlignment = StringAlignment.Center
          });
          break;
        }
        graphics.DrawString("Off", new Font("Segoe UI", 7f, FontStyle.Regular), Brushes.Gray, (float) checked (this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.X + 18), (float) this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Y, new StringFormat()
        {
          Alignment = StringAlignment.Center,
          LineAlignment = StringAlignment.Center
        });
        break;
      case \u200E‬‫⁫‏⁫‌⁫‍‮‎‪‮‎‪⁬​⁫⁫‍⁫‪⁬‮​‍‪‭⁮‭⁪⁬⁮⁫‮‍‌⁮‎‫‮.\u206D‭‪⁬‫⁯⁯‭‫⁬⁪⁮‎‪‪⁮‪‎‏‎​‫‫⁭⁬‬⁭‬‎‪‬‫‮‎⁬‭​⁭⁭‪‮.IO:
        if (this.\u200E⁪‮‏⁬‎‪‏‮‌‪‍⁮​‏⁮⁬‪⁯‍​⁭‏⁫​‏‌⁪‏⁬⁬​⁮‪⁫‪‎⁯‎⁫‮)
        {
          graphics.DrawString("I", new Font("Segoe UI", 7f, FontStyle.Regular), Brushes.Gray, (float) checked (this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.X + 7), (float) this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Y, new StringFormat()
          {
            Alignment = StringAlignment.Center,
            LineAlignment = StringAlignment.Center
          });
          break;
        }
        graphics.DrawString("O", new Font("Segoe UI", 7f, FontStyle.Regular), Brushes.Gray, (float) checked (this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.X + 18), (float) this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Y, new StringFormat()
        {
          Alignment = StringAlignment.Center,
          LineAlignment = StringAlignment.Center
        });
        break;
    }
    graphics.FillEllipse((Brush) new SolidBrush(Color.FromArgb(249, 249, 249)), checked (this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.X + (int) Math.Round(unchecked ((double) this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Width * (double) this.\u206D‍‮‏⁮‭⁮‭‭⁪‮⁪‬‍‭‪‍‮​⁬⁫‍⁯⁫‮⁫‪‭‮⁬​⁬‬‌‪⁯⁫⁯​‪‮ / 80.0)) - (int) Math.Round(unchecked ((double) this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Width / 2.0))), checked (this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Y + (int) Math.Round(unchecked ((double) this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Height / 2.0)) - (int) Math.Round(unchecked ((double) this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Height / 2.0 - 1.0))), this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Width, checked (this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Height - 5));
    graphics.DrawEllipse(new Pen(Color.FromArgb(177, 177, 176)), checked (this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.X + (int) Math.Round(unchecked ((double) this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Width * ((double) this.\u206D‍‮‏⁮‭⁮‭‭⁪‮⁪‬‍‭‪‍‮​⁬⁫‍⁯⁫‮⁫‪‭‮⁬​⁬‬‌‪⁯⁫⁯​‪‮ / 80.0) - (double) checked ((int) Math.Round(unchecked ((double) this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Width / 2.0)))))), checked (this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Y + (int) Math.Round(unchecked ((double) this.\u200B⁭‌‭⁬‫‪‮‮⁪⁬⁪⁪⁮‪⁬​‪‫⁮‮⁮‬‍⁮​⁭⁮‭​⁮​⁬‭⁯‫⁮‫⁫‎‮.Height / 2.0)) - (int) Math.Round(unchecked ((double) this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Height / 2.0 - 1.0))), this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Width, checked (this.\u200D⁪‫⁭‬‏‫‬‍‍⁯⁯‮‪⁫‬⁭⁪‪‬⁬‪⁬⁭‮‭⁯⁭‫⁬‌‫‍⁪‏⁯‍⁮‎‭‮.Height - 5));
  }

  public class \u200E‍‪‌​⁪⁪⁫​‮⁪⁯‮‍‌‬‏⁪⁫⁮‏⁪​⁫‭‌⁯‎‫‎⁬⁫⁮⁫‌⁪‏​​‬‮
  {
    public bool \u202C⁪⁯‫⁯⁭‎‮‪‭⁭⁬‏​⁪‍‏‏⁫⁫⁪‫‬‌‭‌‮‪⁫‪‍⁯⁫‫‌​⁭⁪‭‪‮;
    public bool \u206B​​⁯‮⁭⁯⁪‭‏‮⁭⁫⁯‭⁬‫‮‭‪‎⁪‌⁪‪‍​‎⁮⁯‬‫⁮​‌⁭⁪​‍‏‮;
  }

  public enum \u206D‭‪⁬‫⁯⁯‭‫⁬⁪⁮‎‪‪⁮‪‎‏‎​‫‫⁭⁬‬⁭‬‎‪‬‫‮‎⁬‭​⁭⁭‪‮
  {
    YesNo,
    OnOff,
    IO,
  }

  public delegate void \u206C⁭‏⁬‏‪‪‬⁬‬‫⁫‬⁯‮‮‫‫⁬​‪‎‭⁮​‌⁮‫‍⁭⁮⁯⁪‬⁪⁭⁫⁮‮‮‮();
}
