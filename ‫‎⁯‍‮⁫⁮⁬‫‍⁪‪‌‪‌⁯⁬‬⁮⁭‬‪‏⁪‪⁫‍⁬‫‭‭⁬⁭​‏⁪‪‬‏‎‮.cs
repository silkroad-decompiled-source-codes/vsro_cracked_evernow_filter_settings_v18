﻿// Decompiled with JetBrains decompiler
// Type: ‫‎⁯‍‮⁫⁮⁬‫‍⁪‪‌‪‌⁯⁬‬⁮⁭‬‪‏⁪‪⁫‍⁬‫‭‭⁬⁭​‏⁪‪‬‏‎‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

public class \u202B‎⁯‍‮⁫⁮⁬‫‍⁪‪‌‪‌⁯⁬‬⁮⁭‬‪‏⁪‪⁫‍⁬‫‭‭⁬⁭​‏⁪‪‬‏‎‮ : ContainerControl
{
  public \u202B‎⁯‍‮⁫⁮⁬‫‍⁪‪‌‪‌⁯⁬‬⁮⁭‬‪‏⁪‪⁫‍⁬‫‭‭⁬⁭​‏⁪‪‬‏‎‮()
  {
    this.SetStyle(ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor, true);
    this.BackColor = Color.Transparent;
    this.DoubleBuffered = true;
    this.Size = new Size(212, 104);
    this.MinimumSize = new Size(136, 50);
    this.Padding = new Padding(5, 28, 5, 5);
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    Bitmap bitmap = new Bitmap(this.Width, this.Height);
    Graphics graphics = Graphics.FromImage((Image) bitmap);
    Rectangle rectangle1 = new Rectangle(51, 3, this.Width - 103, 18);
    Rectangle rectangle2 = new Rectangle(0, 0, this.Width - 1, this.Height - 10);
    graphics.Clear(Color.Transparent);
    graphics.SmoothingMode = SmoothingMode.HighQuality;
    graphics.FillPath(Brushes.White, \u202B​‮⁭⁪‍⁫‪‮‏‪‮⁮‬‪⁯⁫‎‎‭⁭‎‬​⁮‌‌‮‎‬⁬⁪⁫⁫‍⁬⁫⁮​⁬‮.\u206A⁭⁮​⁫‬‫‌​⁮‭‬‮‭‫‏⁭⁮‏‭‭‏⁬⁭⁭⁬⁯‫⁬‮⁪⁭⁬⁯⁮‫‫⁪⁯‌‮(new Rectangle(1, 12, this.Width - 3, rectangle2.Height - 1), 8));
    graphics.DrawPath(new Pen(Color.FromArgb(159, 159, 161)), \u202B​‮⁭⁪‍⁫‪‮‏‪‮⁮‬‪⁯⁫‎‎‭⁭‎‬​⁮‌‌‮‎‬⁬⁪⁫⁫‍⁬⁫⁮​⁬‮.\u206A⁭⁮​⁫‬‫‌​⁮‭‬‮‭‫‏⁭⁮‏‭‭‏⁬⁭⁭⁬⁯‫⁬‮⁪⁭⁬⁯⁮‫‫⁪⁯‌‮(new Rectangle(1, 12, this.Width - 3, this.Height - 13), 8));
    graphics.FillPath(Brushes.White, \u202B​‮⁭⁪‍⁫‪‮‏‪‮⁮‬‪⁯⁫‎‎‭⁭‎‬​⁮‌‌‮‎‬⁬⁪⁫⁫‍⁬⁫⁮​⁬‮.\u206A⁭⁮​⁫‬‫‌​⁮‭‬‮‭‫‏⁭⁮‏‭‭‏⁬⁭⁭⁬⁯‫⁬‮⁪⁭⁬⁯⁮‫‫⁪⁯‌‮(rectangle1, 1));
    graphics.DrawPath(new Pen(Color.FromArgb(182, 180, 186)), \u202B​‮⁭⁪‍⁫‪‮‏‪‮⁮‬‪⁯⁫‎‎‭⁭‎‬​⁮‌‌‮‎‬⁬⁪⁫⁫‍⁬⁫⁮​⁬‮.\u206A⁭⁮​⁫‬‫‌​⁮‭‬‮‭‫‏⁭⁮‏‭‭‏⁬⁭⁭⁬⁯‫⁬‮⁪⁭⁬⁯⁮‫‫⁪⁯‌‮(rectangle1, 4));
    graphics.DrawString(this.Text, new Font("Tahoma", 9f, FontStyle.Regular), (Brush) new SolidBrush(Color.FromArgb(53, 53, 53)), (RectangleF) rectangle1, new StringFormat()
    {
      Alignment = StringAlignment.Center,
      LineAlignment = StringAlignment.Center
    });
    _param1.Graphics.DrawImage((Image) bitmap.Clone(), 0, 0);
    graphics.Dispose();
    bitmap.Dispose();
  }
}
