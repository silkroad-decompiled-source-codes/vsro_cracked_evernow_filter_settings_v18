﻿// Decompiled with JetBrains decompiler
// Type: ‭‍⁫‬⁫‍⁬​⁯⁫⁭‬⁭⁮⁮⁯​‏‎⁫⁭⁭‬‭⁭‬‭⁬‏‍⁬‌⁮⁫‭‎⁯⁬‪⁪‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[TypeLibType(4160)]
[Guid("B5E64FFA-C2C5-444E-A301-FB5E00018050")]
[ComImport]
public interface \u202D‍⁫‬⁫‍⁬​⁯⁫⁭‬⁭⁮⁮⁯​‏‎⁫⁭⁭‬‭⁭‬‭⁬‏‍⁬‌⁮⁫‭‎⁯⁬‪⁪‮
{
  [DispId(1)]
  string \u206C⁭‪‫‌‏‪‪‭⁫‭⁬⁭⁭‭⁯‪⁪​‎⁯‪⁪‬⁭‭⁫⁭‫⁭⁭⁯⁪‍‪‌⁬‭‮⁪‮ { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(2)]
  string \u200F​‭⁬‎‭⁭⁮⁮‬⁮⁬⁯​⁯‌‮⁭‫‬‬‪‎‭‌​⁮⁫‬⁬‭⁮‬‪‍‭‎‍⁪⁬‮ { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(3)]
  \u206F‎⁯‍‌⁭‌‮⁮⁫‎⁮⁫‌‏‪​‏‫⁬‏‍‮⁮⁬⁮⁮‫‎⁪‮‪‏‫‫‎​‫‌⁫‮ \u200F‍⁯⁪‍‮‬‌‎‭‍⁫⁮⁭‎⁪‪‍‭‭⁮⁯⁫​‎⁯‮⁫‫⁫‎⁫⁫‍‬‌⁮⁬‏‬‮ { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(4)]
  \u200F⁮‭‪⁫⁭⁪‍​‫⁮⁫​⁫‌⁮‪‌‮‏⁪⁮‪‎⁭⁫‏⁯‏‏‮​⁭‬‌‪‫‬‫‮ \u202C‌‎‫‮‭‎‬⁬⁮‪​‭⁮‬⁭​‫‏‍‌⁫‪⁫⁬‭⁪‍‪‫‫‎⁭‎‫‮‎⁯‎‮‮ { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(5)]
  string \u206C‌⁭‎​‫⁪‭​‪‪⁫⁮⁫‎‪⁫⁭‌⁬‫‬‫‫⁫⁬‭⁯⁭‍‫⁭‪‮⁬‏⁪‎⁯⁪‮ { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(6)]
  bool \u200C‪⁯‬‮‏‪⁪⁭​​⁭‭‫​⁬⁭⁮‌‬⁮‬‫‮‍‪​‮‪⁭‍‬⁪⁬‌⁭⁮⁯⁪‮ { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] set; }
}
