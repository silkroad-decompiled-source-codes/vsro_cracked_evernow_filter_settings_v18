﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: AssemblyTitle("EverNow [Filter Settings]")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Alkayan Groups")]
[assembly: AssemblyProduct("EverNow [Filter Settings]")]
[assembly: AssemblyCopyright("Copyright to Abdelrhman Elbattawy ©  2019")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("e9a6067c-7e9f-40aa-a056-6b9d3e45055a")]
[assembly: AssemblyFileVersion("18.0.0.0")]
[assembly: AssemblyVersion("18.0.0.0")]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
