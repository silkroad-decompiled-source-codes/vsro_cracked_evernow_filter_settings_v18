﻿// Decompiled with JetBrains decompiler
// Type: ‌‏‍‏‪‍‏⁮⁪‫‮‍‪‬⁫‏⁮‍‏‬‎‭‬‍‫⁮‪​⁮⁬‮‬⁪⁯⁯​‪‬‮‏‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Windows.Forms;

public class \u200C‏‍‏‪‍‏⁮⁪‫‮‍‪‬⁫‏⁮‍‏‬‎‭‬‍‫⁮‪​⁮⁬‮‬⁪⁯⁯​‪‬‮‏‮ : ComboBox
{
  public int \u202D⁫⁫‫‮⁮‫‌‎⁫⁬‭⁬‍‫⁯⁭⁮‌‌‬‬⁪‬‮‍‍‪⁭⁬⁮⁪⁮⁭‫‍‌⁯‍‎‮ = 0;
  public Color \u200E‭‪‬⁫‮‏⁫⁪⁯‪⁭⁭​⁮‏⁪‮‬​‮‏⁮‬‪‍⁭‌‫‎‮‎⁮⁮⁯⁮⁫‌⁮‏‮;

  public int \u202C⁫‎‬‏⁮⁬‏‍⁭‬⁮⁭‬‫‬‌‍⁭‬​‭‏⁮⁮⁮‫⁪‍‍‌‌‮‭‭‫‍‪⁬‪‮
  {
    get
    {
      return this.\u202D⁫⁫‫‮⁮‫‌‎⁫⁬‭⁬‍‫⁯⁭⁮‌‌‬‬⁪‬‮‍‍‪⁭⁬⁮⁪⁮⁭‫‍‌⁯‍‎‮;
    }
    set
    {
      this.\u202D⁫⁫‫‮⁮‫‌‎⁫⁬‭⁬‍‫⁯⁭⁮‌‌‬‬⁪‬‮‍‍‪⁭⁬⁮⁪⁮⁭‫‍‌⁯‍‎‮ = value;
      try
      {
        this.\u200E‭‪​​⁮‎⁪‭‫‪‍‪⁮⁬⁯‎⁬‭‏‪⁮⁬‬‭‬‮⁮⁪⁬‬‎⁪‫‫‎‍⁯⁭⁫‮(value);
      }
      catch
      {
      }
      this.Invalidate();
    }
  }

  public Color \u200F‍⁬⁫‍‭‬‏⁭⁭⁫⁬‬‎⁯‍⁭‌⁭‮⁭⁫‍‎‎‪‍‮⁯‬⁪⁫⁮‏⁪‍‭⁯‍⁮‮
  {
    get
    {
      return this.\u200E‭‪‬⁫‮‏⁫⁪⁯‪⁭⁭​⁮‏⁪‮‬​‮‏⁮‬‪‍⁭‌‫‎‮‎⁮⁮⁯⁮⁫‌⁮‏‮;
    }
    set
    {
      this.\u200E‭‪‬⁫‮‏⁫⁪⁯‪⁭⁭​⁮‏⁪‮‬​‮‏⁮‬‪‍⁭‌‫‎‮‎⁮⁮⁯⁮⁫‌⁮‏‮ = value;
      this.Invalidate();
    }
  }

  void ComboBox.\u202E‎‏⁮⁯‏​‮‍‏‬‌⁯⁯‏‪‍⁯‫‪‪​‮⁫⁫⁮⁯⁫‏⁮‪‏‪⁭‮‏‍‪‎‮‮(DrawItemEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((ComboBox) this).OnDrawItem(_param1));
    LinearGradientBrush linearGradientBrush = new LinearGradientBrush(_param1.Bounds, Color.FromArgb(37, 46, 59), Color.FromArgb(37, 46, 59), 90f);
    if (Convert.ToInt32((object) (_param1.State & DrawItemState.Selected)) == 1)
    {
      if (_param1.Index != -1)
      {
        _param1.Graphics.FillRectangle((Brush) linearGradientBrush, _param1.Bounds);
        _param1.Graphics.DrawString(this.GetItemText(this.Items[_param1.Index]), _param1.Font, Brushes.WhiteSmoke, (RectangleF) _param1.Bounds);
      }
    }
    else if (_param1.Index != -1)
    {
      _param1.Graphics.FillRectangle((Brush) new SolidBrush(Color.FromArgb(242, 241, 240)), _param1.Bounds);
      _param1.Graphics.DrawString(this.GetItemText(this.Items[_param1.Index]), _param1.Font, Brushes.DimGray, (RectangleF) _param1.Bounds);
    }
    linearGradientBrush.Dispose();
  }

  void ComboBox.\u206E‭⁪⁭‌​‮⁭‌⁫​‫⁯‏‍⁪‎⁫​‍‏‏⁮‫‪⁮‎‌‫⁭⁫‫⁫‍⁭⁫⁪‪‎⁭‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((ComboBox) this).OnLostFocus(_param1));
    this.SuspendLayout();
    this.Update();
    this.ResumeLayout();
  }

  void Control.\u206C‬‏‬‎⁬⁬⁫​‭‍⁭‏‭‭​‏⁮​‏‬‪‌⁬⁭‎‬‭‏‬​‫⁯‌‮‬‭‏⁮⁪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaintBackground(_param1));
  }

  void ComboBox.\u200E⁬⁪‎‮⁬‪⁫⁪‭‪‌⁯‫‬⁪‮​⁫⁮‫⁬⁫‪‮‬‫⁭⁯⁭‪⁬‍​‍‎‫‏⁬‎‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((ComboBox) this).OnResize(_param1));
    if (this.Focused)
      return;
    this.SelectionLength = 0;
  }

  public \u200C‏‍‏‪‍‏⁮⁪‫‮‍‪‬⁫‏⁮‍‏‬‎‭‬‍‫⁮‪​⁮⁬‮‬⁪⁯⁯​‪‬‮‏‮()
  {
    this.SetStyle(ControlStyles.UserPaint | ControlStyles.Opaque | ControlStyles.ResizeRedraw | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
    this.SetStyle(ControlStyles.Selectable, false);
    this.DrawMode = DrawMode.OwnerDrawFixed;
    this.DropDownStyle = ComboBoxStyle.DropDownList;
    this.BackColor = Color.FromArgb(246, 246, 246);
    this.ForeColor = Color.FromArgb(142, 142, 142);
    this.Size = new Size(135, 26);
    this.ItemHeight = 20;
    this.DropDownHeight = 100;
    this.Font = new Font("Segoe UI", 10f, FontStyle.Regular);
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    _param1.Graphics.Clear(this.Parent.BackColor);
    _param1.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
    GraphicsPath path = \u200E⁭‭‪​⁮⁭​⁭‎⁯⁮⁬‬⁮‎‪‫‌‌‮‏‪⁫‍⁬‪‎‮‮‮⁬‏‫⁮‪‭‌‌‫‮.\u206A⁭⁮​⁫‬‫‌​⁮‭‬‮‭‫‏⁭⁮‏‭‭‏⁬⁭⁭⁬⁯‫⁬‮⁪⁭⁬⁯⁮‫‫⁪⁯‌‮(0, 0, this.Width - 1, this.Height - 1, 5);
    LinearGradientBrush linearGradientBrush = new LinearGradientBrush(this.ClientRectangle, Color.FromArgb(253, 252, 252), Color.FromArgb(239, 237, 236), 90f);
    _param1.Graphics.SetClip(path);
    _param1.Graphics.FillRectangle((Brush) linearGradientBrush, this.ClientRectangle);
    _param1.Graphics.ResetClip();
    _param1.Graphics.DrawPath(new Pen(Color.FromArgb(180, 180, 180)), path);
    _param1.Graphics.DrawString(this.Text, this.Font, (Brush) new SolidBrush(Color.FromArgb(76, 76, 97)), (RectangleF) new Rectangle(3, 0, this.Width - 20, this.Height), new StringFormat()
    {
      LineAlignment = StringAlignment.Center,
      Alignment = StringAlignment.Near
    });
    _param1.Graphics.DrawString("6", new Font("Marlett", 13f, FontStyle.Regular), (Brush) new SolidBrush(Color.FromArgb(119, 119, 118)), (RectangleF) new Rectangle(3, 0, this.Width - 4, this.Height), new StringFormat()
    {
      LineAlignment = StringAlignment.Center,
      Alignment = StringAlignment.Far
    });
    _param1.Graphics.DrawLine(new Pen(Color.FromArgb(224, 222, 220)), this.Width - 24, 4, this.Width - 24, this.Height - 5);
    _param1.Graphics.DrawLine(new Pen(Color.FromArgb(250, 249, 249)), this.Width - 25, 4, this.Width - 25, this.Height - 5);
    path.Dispose();
    linearGradientBrush.Dispose();
  }

  void \u200E‭‪​​⁮‎⁪‭‫‪‍‪⁮⁬⁯‎⁬‭‏‪⁮⁬‬‭‬‮⁮⁪⁬‬‎⁪‫‫‎‍⁯⁭⁫‮([In] int obj0)
  {
    this.SelectedIndex = obj0;
  }
}
