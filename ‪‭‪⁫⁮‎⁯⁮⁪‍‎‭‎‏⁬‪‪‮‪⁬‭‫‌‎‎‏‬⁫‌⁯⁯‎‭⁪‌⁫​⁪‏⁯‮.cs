﻿// Decompiled with JetBrains decompiler
// Type: ‪‭‪⁫⁮‎⁯⁮⁪‍‎‭‎‏⁬‪‪‮‪⁬‭‫‌‎‎‏‬⁫‌⁯⁯‎‭⁪‌⁫​⁪‏⁯‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

[DefaultEvent("TextChanged")]
internal class \u202A‭‪⁫⁮‎⁯⁮⁪‍‎‭‎‏⁬‪‪‮‪⁬‭‫‌‎‎‏‬⁫‌⁯⁯‎‭⁪‌⁫​⁪‏⁯‮ : Control
{
  public RichTextBox \u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮ = new RichTextBox();
  private bool \u206C‏‍⁫‌‪‬‭‍⁯⁬⁪‌‬‫⁮​⁭⁬‪‍⁫​‎⁪‌⁭‪‏‎⁬‬‍‍⁪‏‏‎‌‍‮;
  private bool \u200D‏⁫‍⁭⁬⁮‫‏‎‏‌‬⁫‫⁯‬⁬‌⁫‬‎‍⁯‎⁫⁭‮‍‬‏‍‍⁮‮‫⁮⁪‏‪‮;
  private bool \u202D‮⁫‫‍‌‎⁬‍⁭‫⁫‬‭⁫‪⁫⁮‭‎‌⁫⁫‭‎‭‌⁯⁯‫‍​​‎⁫‮⁯‪​‪‮;
  private GraphicsPath \u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;

  public virtual string \u200F‬‫‮⁫⁯‏⁪‎‪‏⁮‬‌‮‬‫‏‌​‫⁯⁪⁬‎‬⁫‬‬⁬⁮‪‭⁯⁬‫‫⁯‍‌‮
  {
    get
    {
      return this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮.Text;
    }
    set
    {
      this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮.Text = value;
      this.Invalidate();
    }
  }

  public bool \u200C⁯​‭‍‍‪⁭‭‍‪⁯⁮​⁬⁫‭​‫⁫‏​⁯‬‮⁮‍‍⁮‮⁯‎⁬‎⁫⁯⁪‏‫⁮‮
  {
    get
    {
      return this.\u206C‏‍⁫‌‪‬‭‍⁯⁬⁪‌‬‫⁮​⁭⁬‪‍⁫​‎⁪‌⁭‪‏‎⁬‬‍‍⁪‏‏‎‌‍‮;
    }
    set
    {
      this.\u206C‏‍⁫‌‪‬‭‍⁯⁬⁪‌‬‫⁮​⁭⁬‪‍⁫​‎⁪‌⁭‪‏‎⁬‬‍‍⁪‏‏‎‌‍‮ = value;
      if (this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮ == null)
        return;
      this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮.ReadOnly = value;
    }
  }

  public bool \u206F​‌⁭‏⁫‭​‍‬‭‍‫⁭⁯‌⁫​⁪‏⁪‪‬‎‮​‎⁮‏⁮⁪⁭⁭​⁬⁫‎‏⁮⁪‮
  {
    get
    {
      return this.\u200D‏⁫‍⁭⁬⁮‫‏‎‏‌‬⁫‫⁯‬⁬‌⁫‬‎‍⁯‎⁫⁭‮‍‬‏‍‍⁮‮‫⁮⁪‏‪‮;
    }
    set
    {
      this.\u200D‏⁫‍⁭⁬⁮‫‏‎‏‌‬⁫‫⁯‬⁬‌⁫‬‎‍⁯‎⁫⁭‮‍‬‏‍‍⁮‮‫⁮⁪‏‪‮ = value;
      if (this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮ == null)
        return;
      this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮.WordWrap = value;
    }
  }

  public bool \u202C⁭‎⁫⁪⁬‎‪‪‬‎‏‍⁪⁭‬‮‪⁪​‎‫⁬‏⁮⁫‌⁮‎⁬‍⁪‮‮⁭‎‍‭‍‎‮
  {
    get
    {
      return this.\u202D‮⁫‫‍‌‎⁬‍⁭‫⁫‬‭⁫‪⁫⁮‭‎‌⁫⁫‭‎‭‌⁯⁯‫‍​​‎⁫‮⁯‪​‪‮;
    }
    set
    {
      this.\u202D‮⁫‫‍‌‎⁬‍⁭‫⁫‬‭⁫‪⁫⁮‭‎‌⁫⁫‭‎‭‌⁯⁯‫‍​​‎⁫‮⁯‪​‪‮ = value;
      if (this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮ == null)
        return;
      this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮.AutoWordSelection = value;
    }
  }

  void Control.\u202C‮⁭⁫‎‌⁬⁭‮​‍‍⁬‏‭⁫‫⁮⁪‮‭‌‌‏⁮‏‫‪‪⁫‬⁯‍⁫⁭‍​‍⁮⁭‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnTextChanged(_param1));
    this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮.Text = ((Control) this).Text;
    this.Invalidate();
  }

  void Control.\u206A‮‭‭⁯‬‎⁪⁭‬⁯⁭‎⁬‭‭⁮‌⁭‍‬‭‪‬​⁭‏‭‭‎‌⁮‫‭⁬‌‬⁬⁮‫‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnForeColorChanged(_param1));
    this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮.ForeColor = this.ForeColor;
    this.Invalidate();
  }

  void Control.\u200E‪⁯‏⁮‎‮‍‎‭‭⁯‍‬⁪‭‬⁭‪‌⁪⁬​⁮‏‍⁯⁪‌‍‮‌‍⁫⁭⁫‭‭⁪‮‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnFontChanged(_param1));
    this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮.Font = this.Font;
  }

  void Control.\u206C‬‏‬‎⁬⁬⁫​‭‍⁭‏‭‭​‏⁮​‏‬‪‌⁬⁭‎‬‭‏‬​‫⁯‌‮‬‭‏⁮⁪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaintBackground(_param1));
  }

  void Control.\u206B‬‭‎⁯⁬‌‫⁮​⁫⁬‌​⁪‎‪​‌‎‎​‌‬⁮‮‪‭​⁯‬⁪‍‍⁬⁪‏⁪⁮‏‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnSizeChanged(_param1));
    this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮.Size = new Size(this.Width - 13, this.Height - 11);
  }

  void Control.\u200E⁬⁪‎‮⁬‪⁫⁪‭‪‌⁯‫‬⁪‮​⁫⁮‫⁬⁫‪‮‬‫⁭⁯⁭‪⁬‍​‍‎‫‏⁬‎‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnResize(_param1));
    this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮ = new GraphicsPath();
    GraphicsPath graphicsPath = this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;
    graphicsPath.AddArc(0, 0, 10, 10, 180f, 90f);
    graphicsPath.AddArc(this.Width - 11, 0, 10, 10, -90f, 90f);
    graphicsPath.AddArc(this.Width - 11, this.Height - 11, 10, 10, 0.0f, 90f);
    graphicsPath.AddArc(0, this.Height - 11, 10, 10, 90f, 90f);
    graphicsPath.CloseAllFigures();
  }

  public void \u202B⁫‬⁬⁯⁮‍‎⁭⁬⁫⁫‬​‎‍⁭‍‬‏‫‭‫‪⁭‎⁯‎​‍‪‏⁬⁫⁯⁯⁮⁬⁮⁯‮()
  {
    RichTextBox richTextBox = this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮;
    richTextBox.BackColor = Color.White;
    richTextBox.Size = new Size(this.Width - 10, 100);
    richTextBox.Location = new Point(7, 5);
    richTextBox.Text = string.Empty;
    richTextBox.BorderStyle = BorderStyle.None;
    richTextBox.Font = new Font("Tahoma", 10f);
    richTextBox.Multiline = true;
  }

  public \u202A‭‪⁫⁮‎⁯⁮⁪‍‎‭‎‏⁬‪‪‮‪⁬‭‫‌‎‎‏‬⁫‌⁯⁯‎‭⁪‌⁫​⁪‏⁯‮()
  {
    this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
    this.SetStyle(ControlStyles.UserPaint, true);
    this.\u202B⁫‬⁬⁯⁮‍‎⁭⁬⁫⁫‬​‎‍⁭‍‬‏‫‭‫‪⁭‎⁯‎​‍‪‏⁬⁫⁯⁯⁮⁬⁮⁯‮();
    this.Controls.Add((Control) this.\u206D‬‭‎‫‍⁪​⁬⁮‍‬‪⁪‬‎‏‪⁭⁮‌‎⁫‎‮‭‪⁯⁬‍‏‏‮‌⁯⁫⁫‪‌⁪‮);
    this.BackColor = Color.Transparent;
    this.ForeColor = Color.DimGray;
    ((Control) this).Text = (string) null;
    this.Font = new Font("Tahoma", 10f);
    this.Size = new Size(150, 100);
    this.\u206F​‌⁭‏⁫‭​‍‬‭‍‫⁭⁯‌⁫​⁪‏⁪‪‬‎‮​‎⁮‏⁮⁪⁭⁭​⁬⁫‎‏⁮⁪‮ = true;
    this.\u202C⁭‎⁫⁪⁬‎‪‪‬‎‏‍⁪⁭‬‮‪⁪​‎‫⁬‏⁮⁫‌⁮‎⁬‍⁪‮‮⁭‎‍‭‍‎‮ = false;
    this.DoubleBuffered = true;
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    Bitmap bitmap = new Bitmap(this.Width, this.Height);
    Graphics graphics = Graphics.FromImage((Image) bitmap);
    graphics.SmoothingMode = SmoothingMode.AntiAlias;
    graphics.Clear(Color.Transparent);
    graphics.FillPath(Brushes.White, this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
    graphics.DrawPath(new Pen(Color.FromArgb(180, 180, 180)), this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
    graphics.Dispose();
    _param1.Graphics.DrawImage((Image) bitmap.Clone(), 0, 0);
    bitmap.Dispose();
  }
}
