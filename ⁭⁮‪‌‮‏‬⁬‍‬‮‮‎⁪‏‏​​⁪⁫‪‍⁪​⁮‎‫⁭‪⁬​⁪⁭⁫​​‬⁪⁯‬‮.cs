﻿// Decompiled by Syinea's Decompiler
// Type: ⁭⁮‪‌‮‏‬⁬‍‬‮‮‎⁪‏‏​​⁪⁫‪‍⁪​⁮‎‫⁭‪⁬​⁪⁭⁫​​‬⁪⁯‬‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

internal class \u206D⁮‪‌‮‏‬⁬‍‬‮‮‎⁪‏‏​​⁪⁫‪‍⁪​⁮‎‫⁭‪⁬​⁪⁭⁫​​‬⁪⁯‬‮ : Control
{
  private readonly SolidBrush \u202C⁫‭⁬​⁯‌⁯⁬‍‌⁪⁫⁫⁪‫​⁮⁪⁮‪⁪‫‫⁫‭‭⁬⁭⁯⁬‬⁪‬‌​⁫⁭‌‍‮ = new SolidBrush(Color.DarkGray);
  private readonly SolidBrush \u206B‭‪‏‮‌​⁮‫‭‫‫‌‍⁪⁪‮‪‎‫‮‎⁫‪‮⁭⁬⁮‎‌⁫‍‮‪‍‮‭⁭‪⁭‮ = new SolidBrush(Color.DimGray);
  private readonly Timer \u206F‏⁬‮‬‭‌⁪‫‬‫​⁭‭‪‎‮⁫‬⁪‍‬‮‫⁮‫‍⁫‍‍⁬⁫‍‬⁯‏‍‌‪⁫‮ = new Timer();
  private readonly BufferedGraphicsContext \u206C⁮‍‪‎⁯‫⁮⁪‭⁪​⁫‮⁭⁬‫​‎‭‪‬​‏‏‪‮‌‫⁯⁭⁭⁭⁬⁫⁭⁯⁫⁯⁮‮ = BufferedGraphicsManager.Current;
  private PointF[] \u206D⁫‫⁭⁬​‎⁫⁭‬‪‬⁬‭⁭⁫⁪⁫‍‎‬‮⁪‪‬‌‏‭⁪⁪⁮‪‬‬⁫​​‎‍‏‮;
  private BufferedGraphics \u206B⁬​⁯‏‍⁯⁪‬⁯⁫‫⁭‍⁫‎‫⁭⁯​‌‌⁫⁭​​‭‪‭‬⁮‌⁯⁫‬‬⁯‫⁬‫‮;
  private int \u200C‌‭‍⁫‌‎‬​⁪⁭‪‭⁪‬‎⁪‪‫‪‮⁬⁪‮‪⁬⁫⁯‌‭⁪‌‭‌⁭‎⁭‮‍⁮‮;
  private double \u202A‬‎⁬​⁮‭⁮⁪​⁮‪‪⁮​‫⁭‫‏‎‪‌⁪⁫⁯‎‏‏​⁫​⁯⁬‎⁭‌⁪‭⁮‭‮;
  private double \u202A⁯‏‬⁪‫‌‬⁯⁭‏‎‌‎‬​⁯⁪‫​‫⁪‏⁭‪⁮‫‎‏⁪‪‏⁯​⁪‪‭⁪‏‫‮;
  private PointF \u206A‫⁬‭‎‏‏‍⁪⁬‌‍⁯⁮‪‍‮⁯⁪‬⁭⁫‬‏⁮⁬⁬​⁮‍⁭‪‌​⁪⁯‌​‬‌‮;

  public Color \u200C⁪‭‏‪​⁭⁫‌‬​⁬⁬⁭‭⁯⁭​⁯⁪‬‪⁮⁯‍⁪​‭‪⁬‎‪⁫‍⁬‫⁯‭‬‍‮
  {
    get
    {
      return this.\u202C⁫‭⁬​⁯‌⁯⁬‍‌⁪⁫⁫⁪‫​⁮⁪⁮‪⁪‫‫⁫‭‭⁬⁭⁯⁬‬⁪‬‌​⁫⁭‌‍‮.Color;
    }
    set
    {
      this.\u202C⁫‭⁬​⁯‌⁯⁬‍‌⁪⁫⁫⁪‫​⁮⁪⁮‪⁪‫‫⁫‭‭⁬⁭⁯⁬‬⁪‬‌​⁫⁭‌‍‮.Color = value;
    }
  }

  public Color \u206B‌⁪‮⁯‮‫‬‮‪⁭⁮‮⁯‬​​‍⁮⁮⁪‪‌​⁪‍‏​​⁫‮‌‪‌‎‌‎‬‏‬‮
  {
    get
    {
      return this.\u206B‭‪‏‮‌​⁮‫‭‫‫‌‍⁪⁪‮‪‎‫‮‎⁫‪‮⁭⁬⁮‎‌⁫‍‮‪‍‮‭⁭‪⁭‮.Color;
    }
    set
    {
      this.\u206B‭‪‏‮‌​⁮‫‭‫‫‌‍⁪⁪‮‪‎‫‮‎⁫‪‮⁭⁬⁮‎‌⁫‍‮‪‍‮‭⁭‪⁭‮.Color = value;
    }
  }

  public int \u206E⁪‪‌​⁬‬⁯‍‎⁪‪‮⁬⁮‎⁮‭‮⁯⁬⁭⁬‮⁭⁯​‫⁭‏​⁭‬‭‭‮⁪‮​⁯‮
  {
    get
    {
      return this.\u206F‏⁬‮‬‭‌⁪‫‬‫​⁭‭‪‎‮⁫‬⁪‍‬‮‫⁮‫‍⁫‍‍⁬⁫‍‬⁯‏‍‌‪⁫‮.Interval;
    }
    set
    {
      this.\u206F‏⁬‮‬‭‌⁪‫‬‫​⁭‭‪‎‮⁫‬⁪‍‬‮‫⁮‫‍⁫‍‍⁬⁫‍‬⁯‏‍‌‪⁫‮.Interval = value;
    }
  }

  void Control.\u206B‬‭‎⁯⁬‌‫⁮​⁫⁬‌​⁪‎‪​‌‎‎​‌‬⁮‮‪‭​⁯‬⁪‍‍⁬⁪‏⁪⁮‏‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnSizeChanged(_param1));
    this.\u200E‮‌‏⁬⁫‌‪⁬‍⁮​‍‫​‪‌​​‍⁫⁯​‏⁭⁬​⁯‍‌‍​⁬⁯⁫‏‭⁪‍⁭‮();
    this.\u206A‏‍‍⁪⁬‬⁫‍‪⁭​‭‭​‏⁯​‍⁪‭⁯‬‪⁯​‭‫‮⁮⁫⁬‌‍‫⁭⁯⁭‎‏‮();
    this.\u206B⁭‏‪⁮‭⁪​‮​‎⁭‭‍⁭‮⁭⁭⁯⁬‏‭​​⁭⁭⁭⁬⁫‏‮⁯‌‪‮⁯⁮‭⁯‍‮();
  }

  void Control.\u200D⁫‏⁮⁪⁭‏⁯‌⁮‌⁭‪‪‍‍‍‏⁯⁭⁫‬⁯⁫​⁮‏‏‮⁮‭‏⁭⁭​​⁭‬‎‎‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnEnabledChanged(_param1));
    this.\u206F‏⁬‮‬‭‌⁪‫‬‫​⁭‭‪‎‮⁫‬⁪‍‬‮‫⁮‫‍⁫‍‍⁬⁫‍‬⁯‏‍‌‪⁫‮.Enabled = this.Enabled;
  }

  void Control.\u206F⁬​‮‬‮‫‮‪⁯‬‍‏⁭‪‌⁫‪‪⁮‫‮⁭‎⁯‍‎‎⁬‌⁯‬‎​‍⁯‏‪⁯‬‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnHandleCreated(_param1));
    this.\u206F‏⁬‮‬‭‌⁪‫‬‫​⁭‭‪‎‮⁫‬⁪‍‬‮‫⁮‫‍⁫‍‍⁬⁫‍‬⁯‏‍‌‪⁫‮.Tick += new EventHandler(this.\u206A⁫⁬‭‎⁬​‪⁮​‬‎‎‮⁯‫‫⁪⁮‪⁯‮⁭⁪‫‌‪‍‏‬‏‌⁫⁬⁮‬⁪‮⁯‮‮);
    this.\u206F‏⁬‮‬‭‌⁪‫‬‫​⁭‭‪‎‮⁫‬⁪‍‬‮‫⁮‫‍⁫‍‍⁬⁫‍‬⁯‏‍‌‪⁫‮.Start();
  }

  private void \u206A⁫⁬‭‎⁬​‪⁮​‬‎‎‮⁯‫‫⁪⁮‪⁯‮⁭⁪‫‌‪‍‏‬‏‌⁫⁬⁮‬⁪‮⁯‮‮(object _param1, EventArgs _param2)
  {
    if (this.\u200C‌‭‍⁫‌‎‬​⁪⁭‪‭⁪‬‎⁪‪‫‪‮⁬⁪‮‪⁬⁫⁯‌‭⁪‌‭‌⁭‎⁭‮‍⁮‮.Equals(0))
      this.\u200C‌‭‍⁫‌‎‬​⁪⁭‪‭⁪‬‎⁪‪‫‪‮⁬⁪‮‪⁬⁫⁯‌‭⁪‌‭‌⁭‎⁭‮‍⁮‮ = this.\u206D⁫‫⁭⁬​‎⁫⁭‬‪‬⁬‭⁭⁫⁪⁫‍‎‬‮⁪‪‬‌‏‭⁪⁪⁮‪‬‬⁫​​‎‍‏‮.Length - 1;
    else
      --this.\u200C‌‭‍⁫‌‎‬​⁪⁭‪‭⁪‬‎⁪‪‫‪‮⁬⁪‮‪⁬⁫⁯‌‭⁪‌‭‌⁭‎⁭‮‍⁮‮;
    this.Invalidate(false);
  }

  public \u206D⁮‪‌‮‏‬⁬‍‬‮‮‎⁪‏‏​​⁪⁫‪‍⁪​⁮‎‫⁭‪⁬​⁪⁭⁫​​‬⁪⁯‬‮()
  {
    this.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
    this.Size = new Size(80, 80);
    this.Text = string.Empty;
    this.MinimumSize = new Size(80, 80);
    this.\u206B⁭‏‪⁮‭⁪​‮​‎⁭‭‍⁭‮⁭⁭⁯⁬‏‭​​⁭⁭⁭⁬⁫‏‮⁯‌‪‮⁯⁮‭⁯‍‮();
    this.\u206F‏⁬‮‬‭‌⁪‫‬‫​⁭‭‪‎‮⁫‬⁪‍‬‮‫⁮‫‍⁫‍‍⁬⁫‍‬⁯‏‍‌‪⁫‮.Interval = 100;
  }

  private void \u200E‮‌‏⁬⁫‌‪⁬‍⁮​‍‫​‪‌​​‍⁫⁯​‏⁭⁬​⁯‍‌‍​⁬⁯⁫‏‭⁪‍⁭‮()
  {
    int num = Math.Max(this.Width, this.Height);
    this.Size = new Size(num, num);
  }

  private void \u206B⁭‏‪⁮‭⁪​‮​‎⁭‭‍⁭‮⁭⁭⁯⁬‏‭​​⁭⁭⁭⁬⁫‏‮⁯‌‪‮⁯⁮‭⁯‍‮()
  {
    Stack<PointF> pointFStack = new Stack<PointF>();
    PointF pointF1 = new PointF((float) this.Width / 2f, (float) this.Height / 2f);
    for (float num = 0.0f; (double) num < 360.0; num += 45f)
    {
      this.\u202C‎‮⁭⁭‮‏⁭‬‌‎‍‪⁪⁬⁬​‏‌⁮‭⁪⁫⁪‎‪⁯⁫‌‍⁭⁮‏‫‬​‮⁫‍‍‮(pointF1, (int) Math.Round((double) this.Width / 2.0 - 15.0), (double) num);
      PointF pointF2 = this.\u200D⁭‮⁭⁫‭⁮⁯‭‏⁭⁮⁮‫‍‌‌⁯‮‭‍⁬⁮​‌‏‍⁮⁬⁮⁭⁯⁪‪⁫‮‮⁮‪⁭‮;
      pointF2 = new PointF(pointF2.X - 7.5f, pointF2.Y - 7.5f);
      pointFStack.Push(pointF2);
    }
    this.\u206D⁫‫⁭⁬​‎⁫⁭‬‪‬⁬‭⁭⁫⁪⁫‍‎‬‮⁪‪‬‌‏‭⁪⁪⁮‪‬‬⁫​​‎‍‏‮ = pointFStack.ToArray();
  }

  private void \u206A‏‍‍⁪⁬‬⁫‍‪⁭​‭‭​‏⁯​‍⁪‭⁯‬‪⁯​‭‫‮⁮⁫⁬‌‍‫⁭⁯⁭‎‏‮()
  {
    if (this.Width <= 0 || this.Height <= 0)
      return;
    this.\u206C⁮‍‪‎⁯‫⁮⁪‭⁪​⁫‮⁭⁬‫​‎‭‪‬​‏‏‪‮‌‫⁯⁭⁭⁭⁬⁫⁭⁯⁫⁯⁮‮.MaximumBuffer = new Size(this.Width + 1, this.Height + 1);
    this.\u206B⁬​⁯‏‍⁯⁪‬⁯⁫‫⁭‍⁫‎‫⁭⁯​‌‌⁫⁭​​‭‪‭‬⁮‌⁯⁫‬‬⁯‫⁬‫‮ = this.\u206C⁮‍‪‎⁯‫⁮⁪‭⁪​⁫‮⁭⁬‫​‎‭‪‬​‏‏‪‮‌‫⁯⁭⁭⁭⁬⁫⁭⁯⁫⁯⁮‮.Allocate(this.CreateGraphics(), this.ClientRectangle);
    this.\u206B⁬​⁯‏‍⁯⁪‬⁯⁫‫⁭‍⁫‎‫⁭⁯​‌‌⁫⁭​​‭‪‭‬⁮‌⁯⁫‬‬⁯‫⁬‫‮.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    this.\u206B⁬​⁯‏‍⁯⁪‬⁯⁫‫⁭‍⁫‎‫⁭⁯​‌‌⁫⁭​​‭‪‭‬⁮‌⁯⁫‬‬⁯‫⁬‫‮.Graphics.Clear(this.BackColor);
    int num = this.\u206D⁫‫⁭⁬​‎⁫⁭‬‪‬⁬‭⁭⁫⁪⁫‍‎‬‮⁪‪‬‌‏‭⁪⁪⁮‪‬‬⁫​​‎‍‏‮.Length - 1;
    for (int index = 0; index <= num; ++index)
    {
      if (this.\u200C‌‭‍⁫‌‎‬​⁪⁭‪‭⁪‬‎⁪‪‫‪‮⁬⁪‮‪⁬⁫⁯‌‭⁪‌‭‌⁭‎⁭‮‍⁮‮ == index)
        this.\u206B⁬​⁯‏‍⁯⁪‬⁯⁫‫⁭‍⁫‎‫⁭⁯​‌‌⁫⁭​​‭‪‭‬⁮‌⁯⁫‬‬⁯‫⁬‫‮.Graphics.FillEllipse((Brush) this.\u206B‭‪‏‮‌​⁮‫‭‫‫‌‍⁪⁪‮‪‎‫‮‎⁫‪‮⁭⁬⁮‎‌⁫‍‮‪‍‮‭⁭‪⁭‮, this.\u206D⁫‫⁭⁬​‎⁫⁭‬‪‬⁬‭⁭⁫⁪⁫‍‎‬‮⁪‪‬‌‏‭⁪⁪⁮‪‬‬⁫​​‎‍‏‮[index].X, this.\u206D⁫‫⁭⁬​‎⁫⁭‬‪‬⁬‭⁭⁫⁪⁫‍‎‬‮⁪‪‬‌‏‭⁪⁪⁮‪‬‬⁫​​‎‍‏‮[index].Y, 15f, 15f);
      else
        this.\u206B⁬​⁯‏‍⁯⁪‬⁯⁫‫⁭‍⁫‎‫⁭⁯​‌‌⁫⁭​​‭‪‭‬⁮‌⁯⁫‬‬⁯‫⁬‫‮.Graphics.FillEllipse((Brush) this.\u202C⁫‭⁬​⁯‌⁯⁬‍‌⁪⁫⁫⁪‫​⁮⁪⁮‪⁪‫‫⁫‭‭⁬⁭⁯⁬‬⁪‬‌​⁫⁭‌‍‮, this.\u206D⁫‫⁭⁬​‎⁫⁭‬‪‬⁬‭⁭⁫⁪⁫‍‎‬‮⁪‪‬‌‏‭⁪⁪⁮‪‬‬⁫​​‎‍‏‮[index].X, this.\u206D⁫‫⁭⁬​‎⁫⁭‬‪‬⁬‭⁭⁫⁪⁫‍‎‬‮⁪‪‬‌‏‭⁪⁪⁮‪‬‬⁫​​‎‍‏‮[index].Y, 15f, 15f);
    }
    this.\u206B⁬​⁯‏‍⁯⁪‬⁯⁫‫⁭‍⁫‎‫⁭⁯​‌‌⁫⁭​​‭‪‭‬⁮‌⁯⁫‬‬⁯‫⁬‫‮.Render(_param1.Graphics);
  }

  private \u0001 \u206C‎‏​‎⁭⁭‌‏⁫​⁭⁭‌‮‫⁭‍⁯‭⁫⁮⁭‌‌‪⁭‭‭‬‌‌​​⁫⁭⁫⁫‫‌‮<\u0001>(
    ref \u0001 _param1,
    \u0001 _param2)
  {
    _param1 = _param2;
    return _param2;
  }

  private void \u202C‎‮⁭⁭‮‏⁭‬‌‎‍‪⁪⁬⁬​‏‌⁮‭⁪⁫⁪‎‪⁯⁫‌‍⁭⁮‏‫‬​‮⁫‍‍‮(
    PointF _param1,
    int _param2,
    double _param3)
  {
    double num = Math.PI * _param3 / 180.0;
    this.\u206A‫⁬‭‎‏‏‍⁪⁬‌‍⁯⁮‪‍‮⁯⁪‬⁭⁫‬‏⁮⁬⁬​⁮‍⁭‪‌​⁪⁯‌​‬‌‮ = _param1;
    this.\u202A‬‎⁬​⁮‭⁮⁪​⁮‪‪⁮​‫⁭‫‏‎‪‌⁪⁫⁯‎‏‏​⁫​⁯⁬‎⁭‌⁪‭⁮‭‮ = this.\u206C‎‏​‎⁭⁭‌‏⁫​⁭⁭‌‮‫⁭‍⁯‭⁫⁮⁭‌‌‪⁭‭‭‬‌‌​​⁫⁭⁫⁫‫‌‮<double>(ref this.\u202A⁯‏‬⁪‫‌‬⁯⁭‏‎‌‎‬​⁯⁪‫​‫⁪‏⁭‪⁮‫‎‏⁪‪‏⁯​⁪‪‭⁪‏‫‮, (double) _param2);
    this.\u202A‬‎⁬​⁮‭⁮⁪​⁮‪‪⁮​‫⁭‫‏‎‪‌⁪⁫⁯‎‏‏​⁫​⁯⁬‎⁭‌⁪‭⁮‭‮ = Math.Sin(num) * this.\u202A‬‎⁬​⁮‭⁮⁪​⁮‪‪⁮​‫⁭‫‏‎‪‌⁪⁫⁯‎‏‏​⁫​⁯⁬‎⁭‌⁪‭⁮‭‮;
    this.\u202A⁯‏‬⁪‫‌‬⁯⁭‏‎‌‎‬​⁯⁪‫​‫⁪‏⁭‪⁮‫‎‏⁪‪‏⁯​⁪‪‭⁪‏‫‮ = Math.Cos(num) * this.\u202A⁯‏‬⁪‫‌‬⁯⁭‏‎‌‎‬​⁯⁪‫​‫⁪‏⁭‪⁮‫‎‏⁪‪‏⁯​⁪‪‭⁪‏‫‮;
  }

  private PointF \u200D⁭‮⁭⁫‭⁮⁯‭‏⁭⁮⁮‫‍‌‌⁯‮‭‍⁬⁮​‌‏‍⁮⁬⁮⁭⁯⁪‪⁫‮‮⁮‪⁭‮
  {
    get
    {
      return new PointF(Convert.ToSingle((double) this.\u206A‫⁬‭‎‏‏‍⁪⁬‌‍⁯⁮‪‍‮⁯⁪‬⁭⁫‬‏⁮⁬⁬​⁮‍⁭‪‌​⁪⁯‌​‬‌‮.X + this.\u202A⁯‏‬⁪‫‌‬⁯⁭‏‎‌‎‬​⁯⁪‫​‫⁪‏⁭‪⁮‫‎‏⁪‪‏⁯​⁪‪‭⁪‏‫‮), Convert.ToSingle((double) this.\u206A‫⁬‭‎‏‏‍⁪⁬‌‍⁯⁮‪‍‮⁯⁪‬⁭⁫‬‏⁮⁬⁬​⁮‍⁭‪‌​⁪⁯‌​‬‌‮.Y + this.\u202A‬‎⁬​⁮‭⁮⁪​⁮‪‪⁮​‫⁭‫‏‎‪‌⁪⁫⁯‎‏‏​⁫​⁯⁬‎⁭‌⁪‭⁮‭‮));
    }
  }
}
