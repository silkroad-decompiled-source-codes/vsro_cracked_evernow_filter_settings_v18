﻿// Decompiled with JetBrains decompiler
// Type: ‍⁯‍‏‬‍‏⁫⁮‭⁭⁭‪‬‭‏⁪‭⁬⁯​‮‎⁫‮⁬‫⁭‪‏⁭⁭‬​​‪‎‍‏‮‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("79FD57C8-908E-4A36-9888-D5B3F0A444CF")]
[TypeLibType(4160)]
[ComImport]
public interface \u200D⁯‍‏‬‍‏⁫⁮‭⁭⁭‪‬‭‏⁪‭⁬⁯​‮‎⁫‮⁬‫⁭‪‏⁭⁭‬​​‪‎‍‏‮‮
{
  [DispId(1)]
  string \u206C⁭‪‫‌‏‪‪‭⁫‭⁬⁭⁭‭⁯‪⁪​‎⁯‪⁪‬⁭‭⁫⁭‫⁭⁭⁯⁪‍‪‌⁬‭‮⁪‮ { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; }

  [DispId(2)]
  \u200C‎‎‫‫‎‎‌‏⁯‌‎⁬⁮‪⁬⁯​‏⁮‬‎‭⁮‪‌‌⁫‮‬⁭‏⁯‌‏‪​‪⁬⁮‮ \u206D⁪‭‌‏​‍⁫⁭‌‎‫‬‪​⁪‬‪​‮⁭‌‎⁮​‎⁮‫⁯‏‎‏⁯‍⁮‪⁯⁬⁯⁮‮ { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(3)]
  bool \u206B‭⁭⁮‮⁬‏‪‎⁪‎‮‍‏⁫‫⁭‬⁮⁫‭​‭‍​⁯‪‪‫‫⁭‍‪⁮‬‌‮​‎⁪‮ { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(4)]
  \u206F‎⁯‍‌⁭‌‮⁮⁫‎⁮⁫‌‏‪​‏‫⁬‏‍‮⁮⁬⁮⁮‫‎⁪‮‪‏‫‫‎​‫‌⁫‮ \u200F‍⁯⁪‍‮‬‌‎‭‍⁫⁮⁭‎⁪‪‍‭‭⁮⁯⁫​‎⁯‮⁫‫⁫‎⁫⁫‍‬‌⁮⁬‏‬‮ { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(5)]
  \u200F⁮‭‪⁫⁭⁪‍​‫⁮⁫​⁫‌⁮‪‌‮‏⁪⁮‪‎⁭⁫‏⁯‏‏‮​⁭‬‌‪‫‬‫‮ \u202C‌‎‫‮‭‎‬⁬⁮‪​‭⁮‬⁭​‫‏‍‌⁫‪⁫⁬‭⁪‍‪‫‫‎⁭‎‫‮‎⁯‎‮‮ { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(6)]
  string \u206C‌⁭‎​‫⁪‭​‪‪⁫⁮⁫‎‪⁫⁭‌⁬‫‬‫‫⁫⁬‭⁯⁭‍‫⁭‪‮⁬‏⁪‎⁯⁪‮ { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] [param: MarshalAs(UnmanagedType.BStr)] set; }

  [DispId(7)]
  bool \u200C‪⁯‬‮‏‪⁪⁭​​⁭‭‫​⁬⁭⁮‌‬⁮‬‫‮‍‪​‮‪⁭‍‬⁪⁬‌⁭⁮⁯⁪‮ { [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(8)]
  \u206D‌‪⁯‍⁪⁪⁬‌‏⁪⁪⁮‌‏‫⁬‏‏⁭⁮⁯‌‏‌‪‬‏‎‫⁮⁬⁬‫⁬‫⁬⁭‎‬‮ \u206E⁬‏⁬⁭⁮⁭⁯⁯‭‌‍‍‮‎⁫‪‬⁬‫‭⁬‎⁮‮‎‎‬⁫⁯‏‬‭‎⁬‍‫⁭‎⁬‮ { [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] [return: MarshalAs(UnmanagedType.Interface)] get; }
}
