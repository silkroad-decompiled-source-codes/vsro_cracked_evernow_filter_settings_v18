﻿// Decompiled with JetBrains decompiler
// Type: ⁬‪‍‏‬‏‪⁯‮​⁭‬‏⁯‎⁪‌​‪‌⁮⁯‏‭⁬⁪‮⁯‏⁮‮⁯‬‏‭⁬‭⁯⁯‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

[DefaultEvent("CheckedChanged")]
internal class \u206C‪‍‏‬‏‪⁯‮​⁭‬‏⁯‎⁪‌​‪‌⁮⁯‏‭⁬⁪‮⁯‏⁮‮⁯‬‏‭⁬‭⁯⁯‮ : Control
{
  private GraphicsPath \u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;
  private LinearGradientBrush \u206C‌‪‌‬⁬‭​‏‏​‫​‌⁫⁪​‏⁫⁫‏‏‫‏‏⁫⁫⁮‌‏⁪⁮‭​‌‌‍‌‎‭‮;
  private Rectangle \u202D‭​⁭⁮‬⁬⁫⁪⁬‎‎​⁪‍‍‍‌⁯‬⁪‮⁭‏⁯‪⁮‪‫⁯‫‫‏‬‬‬⁫‏‌‏‮;
  private Rectangle \u200C‌⁭‌⁬‬‪‍⁮‌‪⁬⁫‪⁭‌⁮‍⁪‎‌‫⁫⁫⁪‮‎‏‬‬‎‪​⁮‌‏⁯​‏‌‮;
  private bool \u202B⁬‫‪‎‍⁫‌‮⁯‍‎⁭‮⁭‮‫‌‍‬⁯​⁫‏​⁪‭‏⁭‮⁯‭⁮⁫⁮‏⁯⁮⁪‮‮;

  public event \u206C‪‍‏‬‏‪⁯‮​⁭‬‏⁯‎⁪‌​‪‌⁮⁯‏‭⁬⁪‮⁯‏⁮‮⁯‬‏‭⁬‭⁯⁯‮.\u202E⁭⁭⁬⁪‬‪‌‎‏‪‪⁭⁪‪‌‫‪‬⁬⁯⁪⁮⁮‏⁫‮‍⁮⁮⁮‮⁯‬⁭⁪‪‍⁫⁬‮ \u206C⁯‪‮‮⁮⁭⁮‎‍‍⁪⁫‭‪⁮‏‫​‏‬‬‏‫‮⁮⁯⁬⁫‪‍⁭⁪‮‪‌⁮⁪‭‮‮;

  public bool \u206A⁯‬‮‎‫‬‪⁭‬‮⁮‏‮⁫‌⁬‏‭‎‌‌‮‬​‌⁯‏​‮‍⁮‎‏‮‮‏‫​‫‮
  {
    get
    {
      return this.\u202B⁬‫‪‎‍⁫‌‮⁯‍‎⁭‮⁭‮‫‌‍‬⁯​⁫‏​⁪‭‏⁭‮⁯‭⁮⁫⁮‏⁯⁮⁪‮‮;
    }
    set
    {
      this.\u202B⁬‫‪‎‍⁫‌‮⁯‍‎⁭‮⁭‮‫‌‍‬⁯​⁫‏​⁪‭‏⁭‮⁯‭⁮⁫⁮‏⁯⁮⁪‮‮ = value;
      if (this.\u206C⁯‪‮‮⁮⁭⁮‎‍‍⁪⁫‭‪⁮‏‫​‏‬‬‏‫‮⁮⁯⁬⁫‪‍⁭⁪‮‪‌⁮⁪‭‮‮ != null)
        this.\u206C⁯‪‮‮⁮⁭⁮‎‍‍⁪⁫‭‪⁮‏‫​‏‬‬‏‫‮⁮⁯⁬⁫‪‍⁭⁪‮‪‌⁮⁪‭‮‮((object) this);
      this.Invalidate();
    }
  }

  public \u206C‪‍‏‬‏‪⁯‮​⁭‬‏⁯‎⁪‌​‪‌⁮⁯‏‭⁬⁪‮⁯‏⁮‮⁯‬‏‭⁬‭⁯⁯‮()
  {
    this.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
    this.BackColor = Color.Transparent;
    this.DoubleBuffered = true;
    this.Font = new Font("Segoe UI", 10f);
    this.Size = new Size(120, 26);
  }

  void Control.\u206F‍‪‏‭⁭​‍‌⁬‍⁪‏​‪⁬‫‌⁫‬⁪‪⁮‫‮⁮‬‪​⁭‏‌‌⁮‍⁫‬‬‎‮‮(EventArgs _param1)
  {
    this.\u202B⁬‫‪‎‍⁫‌‮⁯‍‎⁭‮⁭‮‫‌‍‬⁯​⁫‏​⁪‭‏⁭‮⁯‭⁮⁫⁮‏⁯⁮⁪‮‮ = !this.\u202B⁬‫‪‎‍⁫‌‮⁯‍‎⁭‮⁭‮‫‌‍‬⁯​⁫‏​⁪‭‏⁭‮⁯‭⁮⁫⁮‏⁯⁮⁪‮‮;
    if (this.\u206C⁯‪‮‮⁮⁭⁮‎‍‍⁪⁫‭‪⁮‏‫​‏‬‬‏‫‮⁮⁯⁬⁫‪‍⁭⁪‮‪‌⁮⁪‭‮‮ != null)
      this.\u206C⁯‪‮‮⁮⁭⁮‎‍‍⁪⁫‭‪⁮‏‫​‏‬‬‏‫‮⁮⁯⁬⁫‪‍⁭⁪‮‪‌⁮⁪‭‮‮((object) this);
    this.Invalidate();
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnClick(_param1));
  }

  void Control.\u202C‮⁭⁫‎‌⁬⁭‮​‍‍⁬‏‭⁫‫⁮⁪‮‭‌‌‏⁮‏‫‪‪⁫‬⁯‍⁫⁭‍​‍⁮⁭‮(EventArgs _param1)
  {
    this.Invalidate();
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnTextChanged(_param1));
  }

  void Control.\u200E⁬⁪‎‮⁬‪⁫⁪‭‪‌⁯‫‬⁪‮​⁫⁮‫⁬⁫‪‮‬‫⁭⁯⁭‪⁬‍​‍‎‫‏⁬‎‮(EventArgs _param1)
  {
    if ((this.Width <= 0 ? 0 : (this.Height > 0 ? 1 : 0)) != 0)
    {
      this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮ = new GraphicsPath();
      this.\u202D‭​⁭⁮‬⁬⁫⁪⁬‎‎​⁪‍‍‍‌⁯‬⁪‮⁭‏⁯‪⁮‪‫⁯‫‫‏‬‬‬⁫‏‌‏‮ = new Rectangle(17, 0, this.Width, this.Height + 1);
      this.\u200C‌⁭‌⁬‬‪‍⁮‌‪⁬⁫‪⁭‌⁮‍⁪‎‌‫⁫⁫⁪‮‎‏‬‬‎‪​⁮‌‏⁯​‏‌‮ = new Rectangle(0, 0, this.Width, this.Height);
      this.\u206C‌‪‌‬⁬‭​‏‏​‫​‌⁫⁪​‏⁫⁫‏‏‫‏‏⁫⁫⁮‌‏⁪⁮‭​‌‌‍‌‎‭‮ = new LinearGradientBrush(new Rectangle(0, 0, 25, 25), Color.FromArgb(250, 250, 250), Color.FromArgb(240, 240, 240), 90f);
      GraphicsPath graphicsPath = this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;
      graphicsPath.AddArc(0, 0, 7, 7, 180f, 90f);
      graphicsPath.AddArc(7, 0, 7, 7, -90f, 90f);
      graphicsPath.AddArc(7, 7, 7, 7, 0.0f, 90f);
      graphicsPath.AddArc(0, 7, 7, 7, 90f, 90f);
      graphicsPath.CloseAllFigures();
      this.Height = 15;
    }
    this.Invalidate();
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnResize(_param1));
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    Graphics graphics = _param1.Graphics;
    graphics.Clear(Color.FromArgb(246, 246, 246));
    graphics.SmoothingMode = SmoothingMode.AntiAlias;
    graphics.FillPath((Brush) this.\u206C‌‪‌‬⁬‭​‏‏​‫​‌⁫⁪​‏⁫⁫‏‏‫‏‏⁫⁫⁮‌‏⁪⁮‭​‌‌‍‌‎‭‮, this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
    graphics.DrawPath(new Pen(Color.FromArgb(160, 160, 160)), this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
    graphics.DrawString(this.Text, this.Font, (Brush) new SolidBrush(Color.FromArgb(142, 142, 142)), (RectangleF) this.\u202D‭​⁭⁮‬⁬⁫⁪⁬‎‎​⁪‍‍‍‌⁯‬⁪‮⁭‏⁯‪⁮‪‫⁯‫‫‏‬‬‬⁫‏‌‏‮, new StringFormat()
    {
      LineAlignment = StringAlignment.Center
    });
    if (this.\u206A⁯‬‮‎‫‬‪⁭‬‮⁮‏‮⁫‌⁬‏‭‎‌‌‮‬​‌⁯‏​‮‍⁮‎‏‮‮‏‫​‫‮)
      graphics.DrawString("ü", new Font("Wingdings", 14f), (Brush) new SolidBrush(Color.FromArgb(142, 142, 142)), (RectangleF) new Rectangle(-2, 1, this.Width, this.Height), new StringFormat()
      {
        LineAlignment = StringAlignment.Center
      });
    _param1.Dispose();
  }

  public delegate void \u202E⁭⁭⁬⁪‬‪‌‎‏‪‪⁭⁪‪‌‫‪‬⁬⁯⁪⁮⁮‏⁫‮‍⁮⁮⁮‮⁯‬⁭⁪‪‍⁫⁬‮(object sender);
}
