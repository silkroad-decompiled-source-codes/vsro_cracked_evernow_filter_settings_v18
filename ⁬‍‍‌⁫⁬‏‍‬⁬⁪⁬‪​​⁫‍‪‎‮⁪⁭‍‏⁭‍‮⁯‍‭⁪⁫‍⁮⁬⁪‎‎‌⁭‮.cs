﻿// Decompiled with JetBrains decompiler
// Type: ⁬‍‍‌⁫⁬‏‍‬⁬⁪⁬‪​​⁫‍‪‎‮⁪⁭‍‏⁭‍‮⁯‍‭⁪⁫‍⁮⁬⁪‎‎‌⁭‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

[DefaultEvent("TextChanged")]
internal class \u206C‍‍‌⁫⁬‏‍‬⁬⁪⁬‪​​⁫‍‪‎‮⁪⁭‍‏⁭‍‮⁯‍‭⁪⁫‍⁮⁬⁪‎‎‌⁭‮ : Control
{
  public TextBox \u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮ = new TextBox();
  private int \u206C‎‏‫⁪⁭‬‌⁭⁬⁮​‪‌‍‍‮⁯‏⁫​‏‮‬‪‫‏‍‪‌⁯‫⁮⁯‪‭⁮‬‎‮‮ = (int) short.MaxValue;
  private bool \u202B⁮‏​‫‪‍‮‫⁬‭⁬‬⁬‏‫⁮‫⁮⁫‪⁬‎⁫⁪‫⁭‮⁮‫‮‏⁭‍​‌⁮‎⁫⁬‮ = false;
  private GraphicsPath \u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;
  private bool \u206C‏‍⁫‌‪‬‭‍⁯⁬⁪‌‬‫⁮​⁭⁬‪‍⁫​‎⁪‌⁭‪‏‎⁬‬‍‍⁪‏‏‎‌‍‮;
  private bool \u202B‎‬‮⁬‫​⁫⁪‏⁫‌⁫‪‪⁮‎⁮‌⁮‎‏⁪‌‬‭‍‮⁫⁫‫‭‮⁮‬‫⁫⁭⁭‮‮;
  private HorizontalAlignment \u202B‮⁪‮⁬⁮‪‌⁫‫⁯⁬⁭‌⁭⁭‍⁫‎⁫‏‭‮⁭⁮‍‪‎‎⁪⁮‬‭⁮‮​⁮⁪‌‌‮;
  private Pen \u202E⁮‭⁫‍⁬⁫‮⁫‎‬​‭‬‬⁮​‎​⁯⁮⁬‌‌⁬⁮⁮⁫‏‮⁯‮⁪‌‎‪‪⁫‬‫‮;
  private SolidBrush \u206E​⁪⁭⁯‫⁪‪‪‫⁯‮⁯‏​⁮‪⁭⁭‏‌‮‍⁫‬‏⁭⁮‬‌‬⁫‫⁮‍‎‬‭⁭‎‮;

  public HorizontalAlignment \u200B⁪‌‪‮⁪⁪‍‬‎⁭‭‍​⁯‌‮‍‭‎‭⁪⁭⁬⁪‎⁪⁬‌⁬⁭‮‏‪‪‪‎⁬⁮‫‮
  {
    get
    {
      return this.\u202B‮⁪‮⁬⁮‪‌⁫‫⁯⁬⁭‌⁭⁭‍⁫‎⁫‏‭‮⁭⁮‍‪‎‎⁪⁮‬‭⁮‮​⁮⁪‌‌‮;
    }
    set
    {
      this.\u202B‮⁪‮⁬⁮‪‌⁫‫⁯⁬⁭‌⁭⁭‍⁫‎⁫‏‭‮⁭⁮‍‪‎‎⁪⁮‬‭⁮‮​⁮⁪‌‌‮ = value;
      this.Invalidate();
    }
  }

  public int \u200E‎⁪⁮‌‍⁯‎‎‎‌⁪‬⁫⁭‪‪‏⁯‬⁬‍⁯‫⁮⁮⁭⁯‫⁫‎⁯‮⁭‏‬⁪⁬‎‬‮
  {
    get
    {
      return this.\u206C‎‏‫⁪⁭‬‌⁭⁬⁮​‪‌‍‍‮⁯‏⁫​‏‮‬‪‫‏‍‪‌⁯‫⁮⁯‪‭⁮‬‎‮‮;
    }
    set
    {
      this.\u206C‎‏‫⁪⁭‬‌⁭⁬⁮​‪‌‍‍‮⁯‏⁫​‏‮‬‪‫‏‍‪‌⁯‫⁮⁯‪‭⁮‬‎‮‮ = value;
      this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.MaxLength = this.\u200E‎⁪⁮‌‍⁯‎‎‎‌⁪‬⁫⁭‪‪‏⁯‬⁬‍⁯‫⁮⁮⁭⁯‫⁫‎⁯‮⁭‏‬⁪⁬‎‬‮;
      this.Invalidate();
    }
  }

  public bool \u200D‫‬‏‏‫⁪‮⁯⁬⁪‬‬‎⁬‮​‫⁯​⁭‫​‫⁪⁯⁮‭‪‮‏⁯‍‌⁮‬‌‏⁬⁬‮
  {
    get
    {
      return this.\u202B⁮‏​‫‪‍‮‫⁬‭⁬‬⁬‏‫⁮‫⁮⁫‪⁬‎⁫⁪‫⁭‮⁮‫‮‏⁭‍​‌⁮‎⁫⁬‮;
    }
    set
    {
      this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.UseSystemPasswordChar = this.\u200D‫‬‏‏‫⁪‮⁯⁬⁪‬‬‎⁬‮​‫⁯​⁭‫​‫⁪⁯⁮‭‪‮‏⁯‍‌⁮‬‌‏⁬⁬‮;
      this.\u202B⁮‏​‫‪‍‮‫⁬‭⁬‬⁬‏‫⁮‫⁮⁫‪⁬‎⁫⁪‫⁭‮⁮‫‮‏⁭‍​‌⁮‎⁫⁬‮ = value;
      this.Invalidate();
    }
  }

  public bool \u200C⁯​‭‍‍‪⁭‭‍‪⁯⁮​⁬⁫‭​‫⁫‏​⁯‬‮⁮‍‍⁮‮⁯‎⁬‎⁫⁯⁪‏‫⁮‮
  {
    get
    {
      return this.\u206C‏‍⁫‌‪‬‭‍⁯⁬⁪‌‬‫⁮​⁭⁬‪‍⁫​‎⁪‌⁭‪‏‎⁬‬‍‍⁪‏‏‎‌‍‮;
    }
    set
    {
      this.\u206C‏‍⁫‌‪‬‭‍⁯⁬⁪‌‬‫⁮​⁭⁬‪‍⁫​‎⁪‌⁭‪‏‎⁬‬‍‍⁪‏‏‎‌‍‮ = value;
      if (this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮ == null)
        return;
      this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.ReadOnly = value;
    }
  }

  public bool \u206D‌‫⁮‏‌‬⁭‏‏‮⁯⁬‫‌‫⁪⁬⁬‫‮​‎‪⁪⁭‭⁮⁪⁪‌‍‫‭‌⁯⁬‎‏⁭‮
  {
    get
    {
      return this.\u202B‎‬‮⁬‫​⁫⁪‏⁫‌⁫‪‪⁮‎⁮‌⁮‎‏⁪‌‬‭‍‮⁫⁫‫‭‮⁮‬‫⁫⁭⁭‮‮;
    }
    set
    {
      this.\u202B‎‬‮⁬‫​⁫⁪‏⁫‌⁫‪‪⁮‎⁮‌⁮‎‏⁪‌‬‭‍‮⁫⁫‫‭‮⁮‬‫⁫⁭⁭‮‮ = value;
      if (this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮ == null)
        return;
      this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.Multiline = value;
      if (value)
        this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.Height = this.Height - 10;
      else
        this.Height = this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.Height + 10;
    }
  }

  void Control.\u202C‮⁭⁫‎‌⁬⁭‮​‍‍⁬‏‭⁫‫⁮⁪‮‭‌‌‏⁮‏‫‪‪⁫‬⁯‍⁫⁭‍​‍⁮⁭‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnTextChanged(_param1));
    this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.Text = this.Text;
    this.Invalidate();
  }

  void Control.\u206A‮‭‭⁯‬‎⁪⁭‬⁯⁭‎⁬‭‭⁮‌⁭‍‬‭‪‬​⁭‏‭‭‎‌⁮‫‭⁬‌‬⁬⁮‫‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnForeColorChanged(_param1));
    this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.ForeColor = this.ForeColor;
    this.Invalidate();
  }

  void Control.\u200E‪⁯‏⁮‎‮‍‎‭‭⁯‍‬⁪‭‬⁭‪‌⁪⁬​⁮‏‍⁯⁪‌‍‮‌‍⁫⁭⁫‭‭⁪‮‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnFontChanged(_param1));
    this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.Font = this.Font;
  }

  void Control.\u206C‬‏‬‎⁬⁬⁫​‭‍⁭‏‭‭​‏⁮​‏‬‪‌⁬⁭‎‬‭‏‬​‫⁯‌‮‬‭‏⁮⁪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaintBackground(_param1));
  }

  private void \u202A‌‌‪‪‎⁫‬⁬‭‌⁪‫⁮⁮⁮​‏‬‪⁬⁯⁮‮⁪⁯‎⁫⁭⁫⁮‪⁫⁮⁯​⁭⁬‪‬‮(object _param1, KeyEventArgs _param2)
  {
    if (_param2.Control && _param2.KeyCode == Keys.A)
    {
      this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.SelectAll();
      _param2.SuppressKeyPress = true;
    }
    if ((!_param2.Control ? 0 : (_param2.KeyCode == Keys.C ? 1 : 0)) == 0)
      return;
    this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.Copy();
    _param2.SuppressKeyPress = true;
  }

  void Control.\u200E⁬⁪‎‮⁬‪⁫⁪‭‪‌⁯‫‬⁪‮​⁫⁮‫⁬⁫‪‮‬‫⁭⁯⁭‪⁬‍​‍‎‫‏⁬‎‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnResize(_param1));
    if (this.\u202B‎‬‮⁬‫​⁫⁪‏⁫‌⁫‪‪⁮‎⁮‌⁮‎‏⁪‌‬‭‍‮⁫⁫‫‭‮⁮‬‫⁫⁭⁭‮‮)
      this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.Height = this.Height - 10;
    else
      this.Height = this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.Height + 10;
    this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮ = new GraphicsPath();
    GraphicsPath graphicsPath = this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;
    graphicsPath.AddArc(0, 0, 10, 10, 180f, 90f);
    graphicsPath.AddArc(this.Width - 11, 0, 10, 10, -90f, 90f);
    graphicsPath.AddArc(this.Width - 11, this.Height - 11, 10, 10, 0.0f, 90f);
    graphicsPath.AddArc(0, this.Height - 11, 10, 10, 90f, 90f);
    graphicsPath.CloseAllFigures();
  }

  void Control.\u200E⁪⁬⁮‬‬⁫‫‏⁫⁪⁭⁭‪‬‍‍‫‮‌⁭⁬⁮⁮‏⁪‬‎⁭⁪‫‏⁫‮⁮‬⁮⁮⁯‌‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnGotFocus(_param1));
    this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.Focus();
  }

  public void \u206D‭‌⁬‌⁫‬⁫‫​‮⁬‌⁯‎‏‭⁫​⁬⁭‏‪⁯⁭⁭⁮​‌‪⁬‎‍‏‏⁭⁯‪⁬‮()
  {
    TextBox textBox = this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮;
    textBox.Size = new Size(this.Width - 10, 33);
    textBox.Location = new Point(7, 5);
    textBox.Text = string.Empty;
    textBox.BorderStyle = BorderStyle.None;
    textBox.TextAlign = HorizontalAlignment.Left;
    textBox.Font = new Font("Tahoma", 11f);
    textBox.UseSystemPasswordChar = this.\u200D‫‬‏‏‫⁪‮⁯⁬⁪‬‬‎⁬‮​‫⁯​⁭‫​‫⁪⁯⁮‭‪‮‏⁯‍‌⁮‬‌‏⁬⁬‮;
    textBox.Multiline = false;
    this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮.KeyDown += new KeyEventHandler(this.\u202A‌‌‪‪‎⁫‬⁬‭‌⁪‫⁮⁮⁮​‏‬‪⁬⁯⁮‮⁪⁯‎⁫⁭⁫⁮‪⁫⁮⁯​⁭⁬‪‬‮);
  }

  public \u206C‍‍‌⁫⁬‏‍‬⁬⁪⁬‪​​⁫‍‪‎‮⁪⁭‍‏⁭‍‮⁯‍‭⁪⁫‍⁮⁬⁪‎‎‌⁭‮()
  {
    this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
    this.SetStyle(ControlStyles.UserPaint, true);
    this.\u206D‭‌⁬‌⁫‬⁫‫​‮⁬‌⁯‎‏‭⁫​⁬⁭‏‪⁯⁭⁭⁮​‌‪⁬‎‍‏‏⁭⁯‪⁬‮();
    this.Controls.Add((Control) this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮);
    this.\u202E⁮‭⁫‍⁬⁫‮⁫‎‬​‭‬‬⁮​‎​⁯⁮⁬‌‌⁬⁮⁮⁫‏‮⁯‮⁪‌‎‪‪⁫‬‫‮ = new Pen(Color.FromArgb(180, 180, 180));
    this.\u206E​⁪⁭⁯‫⁪‪‪‫⁯‮⁯‏​⁮‪⁭⁭‏‌‮‍⁫‬‏⁭⁮‬‌‬⁫‫⁮‍‎‬‭⁭‎‮ = new SolidBrush(Color.White);
    this.BackColor = Color.Transparent;
    this.ForeColor = Color.DimGray;
    this.Text = (string) null;
    this.Font = new Font("Tahoma", 11f);
    this.Size = new Size(135, 33);
    this.DoubleBuffered = true;
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    Bitmap bitmap = new Bitmap(this.Width, this.Height);
    Graphics graphics = Graphics.FromImage((Image) bitmap);
    graphics.SmoothingMode = SmoothingMode.AntiAlias;
    TextBox textBox = this.\u202E‭⁪‎⁯⁯‌​‪‍‏⁭​⁪⁮​‮‮‌‬‏⁪⁫‭​‎⁫​‍⁪‬⁫⁪‬⁬⁬‭‏‮⁭‮;
    textBox.Width = this.Width - 10;
    textBox.TextAlign = this.\u200B⁪‌‪‮⁪⁪‍‬‎⁭‭‍​⁯‌‮‍‭‎‭⁪⁭⁬⁪‎⁪⁬‌⁬⁭‮‏‪‪‪‎⁬⁮‫‮;
    textBox.UseSystemPasswordChar = this.\u200D‫‬‏‏‫⁪‮⁯⁬⁪‬‬‎⁬‮​‫⁯​⁭‫​‫⁪⁯⁮‭‪‮‏⁯‍‌⁮‬‌‏⁬⁬‮;
    graphics.Clear(Color.Transparent);
    graphics.FillPath((Brush) this.\u206E​⁪⁭⁯‫⁪‪‪‫⁯‮⁯‏​⁮‪⁭⁭‏‌‮‍⁫‬‏⁭⁮‬‌‬⁫‫⁮‍‎‬‭⁭‎‮, this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
    graphics.DrawPath(this.\u202E⁮‭⁫‍⁬⁫‮⁫‎‬​‭‬‬⁮​‎​⁯⁮⁬‌‌⁬⁮⁮⁫‏‮⁯‮⁪‌‎‪‪⁫‬‫‮, this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
    _param1.Graphics.DrawImage((Image) bitmap.Clone(), 0, 0);
    graphics.Dispose();
    bitmap.Dispose();
  }
}
