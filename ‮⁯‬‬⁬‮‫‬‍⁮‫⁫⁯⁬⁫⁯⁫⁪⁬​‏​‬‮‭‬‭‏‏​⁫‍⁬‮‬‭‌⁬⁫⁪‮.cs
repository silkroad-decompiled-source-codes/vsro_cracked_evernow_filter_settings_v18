﻿// Decompiled with JetBrains decompiler
// Type: ‮⁯‬‬⁬‮‫‬‍⁮‫⁫⁯⁬⁫⁯⁫⁪⁬​‏​‬‮‭‬‭‏‏​⁫‍⁬‮‬‭‌⁬⁫⁪‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[Guid("9C4C6277-5027-441E-AFAE-CA1F542DA009")]
[TypeLibType(4160)]
[ComImport]
public interface \u202E⁯‬‬⁬‮‫‬‍⁮‫⁫⁯⁬⁫⁯⁫⁪⁬​‏​‬‮‭‬‭‏‏​⁫‍⁬‮‬‭‌⁬⁫⁪‮ : IEnumerable
{
  [DispId(1)]
  int \u202C⁪‎⁬‌⁯‌‭‏‍‭⁭‍‏‭⁫⁮⁪​‬⁪‏‫‎‌⁯‬‏⁯⁯⁭‮‎​‎⁮‬⁬‍⁫‮ { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; }

  [DispId(2)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void \u206A‭‏​‫⁬⁭⁮‭‏⁯‮⁯⁮⁪⁪⁫‭‍⁯⁭⁯‬‮‎⁫⁮‪⁮‍‎⁯⁯⁮⁯‎‪‌⁭⁫‮(
    [MarshalAs(UnmanagedType.Interface), In] \u206F⁫⁬⁭⁯⁫‎‬​‎‍⁮⁯‫‍‮‌‌​‎‮‌‭⁭‎‍‮​⁪‎‌⁯‏‌​⁫⁮‬‬‎‮ _param1);

  [DispId(3)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  void \u202A‍⁬‪⁪⁫​‫‍⁫⁯‏‎‮‍‮‮⁭‫⁮⁫‭‎⁭⁬‏‎⁯⁫⁬‬⁮‮‬‎⁫⁮‮⁭‎‮([MarshalAs(UnmanagedType.BStr), In] string _param1);

  [DispId(4)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.Interface)]
  \u206F⁫⁬⁭⁯⁫‎‬​‎‍⁮⁯‫‍‮‌‌​‎‮‌‭⁭‎‍‮​⁪‎‌⁯‏‌​⁫⁮‬‬‎‮ \u206B‮‮​‮⁬⁮​‫‮⁯‬​‏‮‎⁯⁮‫‮‫‍⁮‬⁬⁪‎​‏‍‌⁬‌​⁬‍⁪‫​‎‮(
    [MarshalAs(UnmanagedType.BStr), In] string _param1);

  [DispId(-4)]
  [TypeLibFunc(1)]
  [MethodImpl(MethodImplOptions.InternalCall)]
  [return: MarshalAs(UnmanagedType.CustomMarshaler)]
  IEnumerator \u206D​⁭‌‍⁬⁫⁮‫‏‎⁫⁭⁯⁫⁭‏‏‌‌‍‍⁯‍‭⁬⁪‭‪‬⁮⁭‮⁪⁭‍​⁪‌‎‮();
}
