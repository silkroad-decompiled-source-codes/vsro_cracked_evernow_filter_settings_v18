﻿// Decompiled with JetBrains decompiler
// Type: ‌‍⁫⁭⁮‬⁪‌‏⁪⁬⁯‌‪‎‪‭⁬⁪‭‮⁭‌​‬‍⁮⁬​‎⁮‮‫‪‍‫⁪‮‌‮‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Windows.Forms;

internal class \u200C‍⁫⁭⁮‬⁪‌‏⁪⁬⁯‌‪‎‪‭⁬⁪‭‮⁭‌​‬‍⁮⁬​‎⁮‮‫‪‍‫⁪‮‌‮‮ : ComboBox
{
  private int \u202D⁫⁫‫‮⁮‫‌‎⁫⁬‭⁬‍‫⁯⁭⁮‌‌‬‬⁪‬‮‍‍‪⁭⁬⁮⁪⁮⁭‫‍‌⁯‍‎‮ = 0;
  private Color \u200E‭‪‬⁫‮‏⁫⁪⁯‪⁭⁭​⁮‏⁪‮‬​‮‏⁮‬‪‍⁭‌‫‎‮‎⁮⁮⁯⁮⁫‌⁮‏‮ = Color.FromArgb(241, 241, 241);

  public int \u202C⁫‎‬‏⁮⁬‏‍⁭‬⁮⁭‬‫‬‌‍⁭‬​‭‏⁮⁮⁮‫⁪‍‍‌‌‮‭‭‫‍‪⁬‪‮
  {
    get
    {
      return this.\u202D⁫⁫‫‮⁮‫‌‎⁫⁬‭⁬‍‫⁯⁭⁮‌‌‬‬⁪‬‮‍‍‪⁭⁬⁮⁪⁮⁭‫‍‌⁯‍‎‮;
    }
    set
    {
      this.\u202D⁫⁫‫‮⁮‫‌‎⁫⁬‭⁬‍‫⁯⁭⁮‌‌‬‬⁪‬‮‍‍‪⁭⁬⁮⁪⁮⁭‫‍‌⁯‍‎‮ = value;
      try
      {
        this.\u200F‭⁯⁭‍⁮‪⁭‪⁪⁬⁫‪⁫‬‭⁪⁫‎⁭⁯⁯‌⁮‪⁬‭⁮⁭⁯⁫⁯‫​‫‌‎‪‍⁬‮(value);
      }
      catch
      {
      }
      this.Invalidate();
    }
  }

  public Color \u200F‍⁬⁫‍‭‬‏⁭⁭⁫⁬‬‎⁯‍⁭‌⁭‮⁭⁫‍‎‎‪‍‮⁯‬⁪⁫⁮‏⁪‍‭⁯‍⁮‮
  {
    get
    {
      return this.\u200E‭‪‬⁫‮‏⁫⁪⁯‪⁭⁭​⁮‏⁪‮‬​‮‏⁮‬‪‍⁭‌‫‎‮‎⁮⁮⁯⁮⁫‌⁮‏‮;
    }
    set
    {
      this.\u200E‭‪‬⁫‮‏⁫⁪⁯‪⁭⁭​⁮‏⁪‮‬​‮‏⁮‬‪‍⁭‌‫‎‮‎⁮⁮⁯⁮⁫‌⁮‏‮ = value;
      this.Invalidate();
    }
  }

  void ComboBox.\u202E‎‏⁮⁯‏​‮‍‏‬‌⁯⁯‏‪‍⁯‫‪‪​‮⁫⁫⁮⁯⁫‏⁮‪‏‪⁭‮‏‍‪‎‮‮(DrawItemEventArgs _param1)
  {
    if ((_param1.State & DrawItemState.Selected) != DrawItemState.Selected)
      _param1.Graphics.FillRectangle(Brushes.White, _param1.Bounds);
    else
      _param1.Graphics.FillRectangle((Brush) new SolidBrush(this.\u200E‭‪‬⁫‮‏⁫⁪⁯‪⁭⁭​⁮‏⁪‮‬​‮‏⁮‬‪‍⁭‌‫‎‮‎⁮⁮⁯⁮⁫‌⁮‏‮), _param1.Bounds);
    if (_param1.Index == -1)
      return;
    _param1.Graphics.DrawString(this.GetItemText(this.Items[_param1.Index]), _param1.Font, Brushes.DimGray, (RectangleF) _param1.Bounds);
  }

  void ComboBox.\u206E‭⁪⁭‌​‮⁭‌⁫​‫⁯‏‍⁪‎⁫​‍‏‏⁮‫‪⁮‎‌‫⁭⁫‫⁫‍⁭⁫⁪‪‎⁭‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((ComboBox) this).OnLostFocus(_param1));
    this.SuspendLayout();
    this.Update();
    this.ResumeLayout();
  }

  void Control.\u206C‬‏‬‎⁬⁬⁫​‭‍⁭‏‭‭​‏⁮​‏‬‪‌⁬⁭‎‬‭‏‬​‫⁯‌‮‬‭‏⁮⁪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaintBackground(_param1));
  }

  public \u200C‍⁫⁭⁮‬⁪‌‏⁪⁬⁯‌‪‎‪‭⁬⁪‭‮⁭‌​‬‍⁮⁬​‎⁮‮‫‪‍‫⁪‮‌‮‮()
  {
    this.SetStyle(ControlStyles.UserPaint | ControlStyles.Opaque | ControlStyles.ResizeRedraw | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
    this.SetStyle(ControlStyles.Selectable, false);
    this.DrawMode = DrawMode.OwnerDrawFixed;
    this.DropDownStyle = ComboBoxStyle.DropDownList;
    this.BackColor = Color.FromArgb(246, 246, 246);
    this.ForeColor = Color.FromArgb(142, 142, 142);
    this.Size = new Size(135, 26);
    this.ItemHeight = 20;
    this.DropDownHeight = 100;
    this.Font = new Font("Segoe UI", 10f, FontStyle.Regular);
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    _param1.Graphics.Clear(this.BackColor);
    _param1.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
    GraphicsPath path = \u202B​‮⁭⁪‍⁫‪‮‏‪‮⁮‬‪⁯⁫‎‎‭⁭‎‬​⁮‌‌‮‎‬⁬⁪⁫⁫‍⁬⁫⁮​⁬‮.\u206A⁭⁮​⁫‬‫‌​⁮‭‬‮‭‫‏⁭⁮‏‭‭‏⁬⁭⁭⁬⁯‫⁬‮⁪⁭⁬⁯⁮‫‫⁪⁯‌‮(0, 0, this.Width - 1, this.Height - 1, 5);
    LinearGradientBrush linearGradientBrush = new LinearGradientBrush(this.ClientRectangle, Color.FromArgb(241, 241, 241), Color.FromArgb(241, 241, 241), 90f);
    _param1.Graphics.SetClip(path);
    _param1.Graphics.FillRectangle((Brush) linearGradientBrush, this.ClientRectangle);
    _param1.Graphics.ResetClip();
    _param1.Graphics.DrawPath(new Pen(Color.FromArgb(204, 204, 204)), path);
    _param1.Graphics.DrawString(this.Text, this.Font, (Brush) new SolidBrush(Color.FromArgb(142, 142, 142)), (RectangleF) new Rectangle(3, 0, this.Width - 20, this.Height), new StringFormat()
    {
      LineAlignment = StringAlignment.Center,
      Alignment = StringAlignment.Near
    });
    _param1.Graphics.DrawLine(new Pen(Color.FromArgb(160, 160, 160), 2f), new Point(this.Width - 18, 10), new Point(this.Width - 14, 14));
    _param1.Graphics.DrawLine(new Pen(Color.FromArgb(160, 160, 160), 2f), new Point(this.Width - 14, 14), new Point(this.Width - 10, 10));
    _param1.Graphics.DrawLine(new Pen(Color.FromArgb(160, 160, 160)), new Point(this.Width - 14, 15), new Point(this.Width - 14, 14));
    path.Dispose();
    linearGradientBrush.Dispose();
  }

  void \u200F‭⁯⁭‍⁮‪⁭‪⁪⁬⁫‪⁫‬‭⁪⁫‎⁭⁯⁯‌⁮‪⁬‭⁮⁭⁯⁫⁯‫​‫‌‎‪‍⁬‮([In] int obj0)
  {
    this.SelectedIndex = obj0;
  }
}
