﻿// Decompiled with JetBrains decompiler
// Type: ⁫‮​‌‬‫‬⁭‎‍⁭⁮‬‏⁪‍‮​⁮⁭‫‫‪‍‌⁬⁮‍⁮⁭‌⁮‭‭⁪‫‭‪‌‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

internal class \u206B‮​‌‬‫‬⁭‎‍⁭⁮‬‏⁪‍‮​⁮⁭‫‫‪‍‌⁬⁮‍⁮⁭‌⁮‭‭⁪‫‭‪‌‮ : TabControl
{
  public \u206B‮​‌‬‫‬⁭‎‍⁭⁮‬‏⁪‍‮​⁮⁭‫‫‪‍‌⁬⁮‍⁮⁭‌⁮‭‭⁪‫‭‪‌‮()
  {
    this.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
    this.DoubleBuffered = true;
    this.SizeMode = TabSizeMode.Fixed;
    this.ItemSize = new Size(44, 135);
    this.DrawMode = TabDrawMode.OwnerDrawFixed;
    foreach (Control tabPage in this.TabPages)
      tabPage.BackColor = Color.FromArgb(246, 246, 246);
  }

  void TabControl.\u200C⁮‍⁭‭‫⁫‍⁪‎‪⁬‎‭​‏⁯⁫⁬‎‫⁭⁪⁮‎⁪⁫‪⁭‪​⁮‫⁭‬‮‌⁫‌⁬‮()
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((TabControl) this).CreateHandle());
    this.DoubleBuffered = true;
    this.SizeMode = TabSizeMode.Fixed;
    this.Appearance = TabAppearance.Normal;
    this.Alignment = TabAlignment.Left;
  }

  void Control.\u202C‪⁬‌⁪‮⁯‎‍‬‫‪‮‏‌‪‏⁭‬‌‮‎⁮​‍‏‫⁭‮⁪⁪‍‍‭⁭‭‏‏‬‮‮(ControlEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnControlAdded(_param1));
    if (!(_param1.Control is TabPage))
      return;
    try
    {
      IEnumerator enumerator = this.Controls.GetEnumerator();
      while (enumerator.MoveNext())
      {
        TabPage current = (TabPage) enumerator.Current;
        TabPage tabPage = new TabPage();
      }
    }
    finally
    {
      _param1.Control.BackColor = Color.FromArgb(246, 246, 246);
    }
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    Bitmap bitmap = new Bitmap(this.Width, this.Height);
    Graphics graphics1 = Graphics.FromImage((Image) bitmap);
    Graphics graphics2 = graphics1;
    graphics2.Clear(Color.FromArgb(246, 246, 246));
    graphics2.SmoothingMode = SmoothingMode.HighSpeed;
    graphics2.CompositingQuality = CompositingQuality.HighSpeed;
    graphics2.CompositingMode = CompositingMode.SourceOver;
    graphics2.FillRectangle((Brush) new SolidBrush(Color.FromArgb(54, 57, 64)), new Rectangle(-5, 0, this.ItemSize.Height + 4, this.Height));
    Graphics graphics3 = graphics2;
    Pen pen = new Pen(Color.FromArgb(25, 26, 28));
    Size itemSize = this.ItemSize;
    int x1 = itemSize.Height - 1;
    itemSize = this.ItemSize;
    int x2 = itemSize.Height - 1;
    int height1 = this.Height;
    graphics3.DrawLine(pen, x1, 0, x2, height1);
    Rectangle tabRect;
    Point location1;
    for (int index = 0; index <= this.TabCount - 1; ++index)
    {
      if (index != this.SelectedIndex)
      {
        Rectangle rectangle;
        ref Rectangle local = ref rectangle;
        tabRect = this.GetTabRect(index);
        location1 = tabRect.Location;
        int x = location1.X - 2;
        tabRect = this.GetTabRect(index);
        location1 = tabRect.Location;
        int y = location1.Y - 2;
        Point location2 = new Point(x, y);
        tabRect = this.GetTabRect(index);
        int width = tabRect.Width + 3;
        tabRect = this.GetTabRect(index);
        int height2 = tabRect.Height - 8;
        Size size = new Size(width, height2);
        local = new Rectangle(location2, size);
        graphics2.DrawString(this.TabPages[index].Text, new Font(this.Font.FontFamily, this.Font.Size, FontStyle.Bold), (Brush) new SolidBrush(Color.FromArgb(159, 162, 167)), (RectangleF) new Rectangle(rectangle.Left + 40, rectangle.Top + 12, rectangle.Width - 40, rectangle.Height), new StringFormat()
        {
          Alignment = StringAlignment.Near
        });
        if (this.ImageList != null && this.TabPages[index].ImageIndex != -1)
          graphics2.DrawImage(this.ImageList.Images[this.TabPages[index].ImageIndex], rectangle.X + 9, rectangle.Y + 6, 24, 24);
      }
      else
      {
        Rectangle rectangle;
        ref Rectangle local1 = ref rectangle;
        tabRect = this.GetTabRect(index);
        location1 = tabRect.Location;
        int x3 = location1.X - 2;
        tabRect = this.GetTabRect(index);
        location1 = tabRect.Location;
        int y1 = location1.Y - 2;
        Point location2 = new Point(x3, y1);
        tabRect = this.GetTabRect(index);
        int width = tabRect.Width + 3;
        tabRect = this.GetTabRect(index);
        int height2 = tabRect.Height - 8;
        Size size1 = new Size(width, height2);
        local1 = new Rectangle(location2, size1);
        graphics2.FillRectangle((Brush) new SolidBrush(Color.FromArgb(35, 36, 38)), rectangle.X, rectangle.Y, rectangle.Width - 4, rectangle.Height + 3);
        Rectangle rect;
        ref Rectangle local2 = ref rect;
        tabRect = this.GetTabRect(index);
        int x4 = tabRect.X - 2;
        tabRect = this.GetTabRect(index);
        location1 = tabRect.Location;
        int y2 = location1.Y - (index != 0 ? 1 : 1);
        Point location3 = new Point(x4, y2);
        tabRect = this.GetTabRect(index);
        Size size2 = new Size(4, tabRect.Height - 7);
        local2 = new Rectangle(location3, size2);
        graphics2.FillRectangle((Brush) new SolidBrush(Color.FromArgb(89, 169, 222)), rect);
        graphics2.DrawString(this.TabPages[index].Text, new Font(this.Font.FontFamily, this.Font.Size, FontStyle.Bold), (Brush) new SolidBrush(Color.FromArgb(254, (int) byte.MaxValue, (int) byte.MaxValue)), (RectangleF) new Rectangle(rectangle.Left + 40, rectangle.Top + 12, rectangle.Width - 40, rectangle.Height), new StringFormat()
        {
          Alignment = StringAlignment.Near
        });
        if (this.ImageList != null && this.TabPages[index].ImageIndex != -1)
          graphics2.DrawImage(this.ImageList.Images[this.TabPages[index].ImageIndex], rectangle.X + 9, rectangle.Y + 6, 24, 24);
      }
    }
    _param1.Graphics.SmoothingMode = SmoothingMode.HighQuality;
    _param1.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
    _param1.Graphics.CompositingQuality = CompositingQuality.HighQuality;
    _param1.Graphics.DrawImage((Image) bitmap.Clone(), 0, 0);
    graphics1.Dispose();
    bitmap.Dispose();
  }
}
