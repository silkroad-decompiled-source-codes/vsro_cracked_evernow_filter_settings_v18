﻿// Decompiled with JetBrains decompiler
// Type: ‭‍‌‭⁫⁯​‮⁭⁯‬‍‍‎​‫‍⁯‬⁬⁪⁮⁭⁪⁫​⁬‎‏‬‏​⁪‬‎⁪‬⁬‬⁭‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

public class \u202D‍‌‭⁫⁯​‮⁭⁯‬‍‍‎​‫‍⁯‬⁬⁪⁮⁭⁪⁫​⁬‎‏‬‏​⁪‬‎⁪‬⁬‬⁭‮ : Control
{
  private Color \u200C‪⁯⁮⁪‫​‫‮⁪‎‏⁮⁮‭⁭​‪⁪⁭‪‏‏⁯⁫⁪​⁯⁮‪‭⁬‮‬⁬⁮‮⁫⁮‮ = Color.FromArgb(52, 52, 52);
  private Color \u206F‭⁬⁪‫‏‭⁯‏​⁬⁫⁪⁫‍​‍⁪⁭⁪​‬‪‪‭‬‎‏⁮‪⁭‭⁪‭⁬‭‏‪‬⁫‮ = Color.FromArgb(192, 206, 215);
  private bool \u202A⁮‏‬‬‭‮⁫‭⁬‍⁬‎‏‌‏⁫⁯‏⁪‏‏‍‭‫⁯‫‬‮‬‮‫‌⁪⁪⁭‫‬‭‮‮ = true;
  private GraphicsPath \u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;

  public virtual Color \u206F⁬⁯‭‎⁮‮‮‪‭⁯‎‬⁭‮‫‌‭‭‪⁪‏‬⁪⁬⁮​‎‎‮‍⁫‫⁭‎‍‎‬‍⁭‮
  {
    get
    {
      return this.\u200C‪⁯⁮⁪‫​‫‮⁪‎‏⁮⁮‭⁭​‪⁪⁭‪‏‏⁯⁫⁪​⁯⁮‪‭⁬‮‬⁬⁮‮⁫⁮‮;
    }
    set
    {
      this.\u200C‪⁯⁮⁪‫​‫‮⁪‎‏⁮⁮‭⁭​‪⁪⁭‪‏‏⁯⁫⁪​⁯⁮‪‭⁬‮‬⁬⁮‮⁫⁮‮ = value;
      this.Invalidate();
    }
  }

  public Color \u200E‭⁬⁭‏‎‎‮⁫‭‏‬⁫‏‏‎‭‪‍‫‪⁯‫‪‌‍​‍⁭‬‪‭‭⁬‏⁯‎⁬‎⁬‮
  {
    get
    {
      return this.\u206F‭⁬⁪‫‏‭⁯‏​⁬⁫⁪⁫‍​‍⁪⁭⁪​‬‪‪‭‬‎‏⁮‪⁭‭⁪‭⁬‭‏‪‬⁫‮;
    }
    set
    {
      this.\u206F‭⁬⁪‫‏‭⁯‏​⁬⁫⁪⁫‍​‍⁪⁭⁪​‬‪‪‭‬‎‏⁮‪⁭‭⁪‭⁬‭‏‪‬⁫‮ = value;
      this.Invalidate();
    }
  }

  public bool \u206A‎‬⁬‭⁮‪‭⁬⁪‫‭⁪‭⁭⁬‎⁯‬‬⁭‌‍‍‮‪⁬‌‎‪‭‪‎‏‌⁫‫⁫‪‏‮
  {
    get
    {
      return this.\u202A⁮‏‬‬‭‮⁫‭⁬‍⁬‎‏‌‏⁫⁯‏⁪‏‏‍‭‫⁯‫‬‮‬‮‫‌⁪⁪⁭‫‬‭‮‮;
    }
    set
    {
      this.\u202A⁮‏‬‬‭‮⁫‭⁬‍⁬‎‏‌‏⁫⁯‏⁪‏‏‍‭‫⁯‫‬‮‬‮‫‌⁪⁪⁭‫‬‭‮‮ = value;
      this.Invalidate();
    }
  }

  public \u202D‍‌‭⁫⁯​‮⁭⁯‬‍‍‎​‫‍⁯‬⁬⁪⁮⁭⁪⁫​⁬‎‏‬‏​⁪‬‎⁪‬⁬‬⁭‮()
  {
    this.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
    this.DoubleBuffered = true;
    this.Size = new Size(152, 38);
    this.BackColor = Color.Transparent;
    ((Control) this).ForeColor = Color.FromArgb(52, 52, 52);
    this.Font = new Font("Segoe UI", 10f);
  }

  void Control.\u200E⁬⁪‎‮⁬‪⁫⁪‭‪‌⁯‫‬⁪‮​⁫⁮‫⁬⁫‪‮‬‫⁭⁯⁭‪⁬‍​‍‎‫‏⁬‎‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnResize(_param1));
    this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮ = new GraphicsPath();
    GraphicsPath graphicsPath = this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;
    graphicsPath.AddArc(0, 0, 10, 10, 180f, 90f);
    graphicsPath.AddArc(this.Width - 18, 0, 10, 10, -90f, 90f);
    graphicsPath.AddArc(this.Width - 18, this.Height - 11, 10, 10, 0.0f, 90f);
    graphicsPath.AddArc(0, this.Height - 11, 10, 10, 90f, 90f);
    graphicsPath.CloseAllFigures();
    this.Invalidate();
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    Bitmap bitmap = new Bitmap(this.Width, this.Height);
    Graphics graphics1 = Graphics.FromImage((Image) bitmap);
    Graphics graphics2 = graphics1;
    graphics2.SmoothingMode = SmoothingMode.HighQuality;
    graphics2.PixelOffsetMode = PixelOffsetMode.HighQuality;
    graphics2.Clear(this.BackColor);
    graphics2.FillPath((Brush) new SolidBrush(this.\u206F‭⁬⁪‫‏‭⁯‏​⁬⁫⁪⁫‍​‍⁪⁭⁪​‬‪‪‭‬‎‏⁮‪⁭‭⁪‭⁬‭‏‪‬⁫‮), this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
    graphics2.DrawString(this.Text, this.Font, (Brush) new SolidBrush(((Control) this).ForeColor), (RectangleF) new Rectangle(6, 4, this.Width - 15, this.Height));
    if (this.\u202A⁮‏‬‬‭‮⁫‭⁬‍⁬‎‏‌‏⁫⁯‏⁪‏‏‍‭‫⁯‫‬‮‬‮‫‌⁪⁪⁭‫‬‭‮‮)
    {
      Point[] points = new Point[3]
      {
        new Point(this.Width - 8, this.Height - 19),
        new Point(this.Width, this.Height - 25),
        new Point(this.Width - 8, this.Height - 30)
      };
      graphics2.FillPolygon((Brush) new SolidBrush(this.\u206F‭⁬⁪‫‏‭⁯‏​⁬⁫⁪⁫‍​‍⁪⁭⁪​‬‪‪‭‬‎‏⁮‪⁭‭⁪‭⁬‭‏‪‬⁫‮), points);
      graphics2.DrawPolygon(new Pen((Brush) new SolidBrush(this.\u206F‭⁬⁪‫‏‭⁯‏​⁬⁫⁪⁫‍​‍⁪⁭⁪​‬‪‪‭‬‎‏⁮‪⁭‭⁪‭⁬‭‏‪‬⁫‮)), points);
    }
    graphics1.Dispose();
    _param1.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
    _param1.Graphics.DrawImageUnscaled((Image) bitmap, 0, 0);
    bitmap.Dispose();
  }
}
