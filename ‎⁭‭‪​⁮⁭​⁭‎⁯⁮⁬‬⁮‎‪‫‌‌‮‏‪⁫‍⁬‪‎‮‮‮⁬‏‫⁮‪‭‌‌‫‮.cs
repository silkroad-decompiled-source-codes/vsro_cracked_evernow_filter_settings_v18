﻿// Decompiled with JetBrains decompiler
// Type: ‎⁭‭‪​⁮⁭​⁭‎⁯⁮⁬‬⁮‎‪‫‌‌‮‏‪⁫‍⁬‪‎‮‮‮⁬‏‫⁮‪‭‌‌‫‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System.Drawing;
using System.Drawing.Drawing2D;

internal static class \u200E⁭‭‪​⁮⁭​⁭‎⁯⁮⁬‬⁮‎‪‫‌‌‮‏‪⁫‍⁬‪‎‮‮‮⁬‏‫⁮‪‭‌‌‫‮
{
  public static GraphicsPath \u206A⁭⁮​⁫‬‫‌​⁮‭‬‮‭‫‏⁭⁮‏‭‭‏⁬⁭⁭⁬⁯‫⁬‮⁪⁭⁬⁯⁮‫‫⁪⁯‌‮(
    Rectangle _param0,
    int _param1)
  {
    GraphicsPath graphicsPath = new GraphicsPath();
    int num = _param1 * 2;
    graphicsPath.AddArc(new Rectangle(_param0.X, _param0.Y, num, num), -180f, 90f);
    graphicsPath.AddArc(new Rectangle(_param0.Width - num + _param0.X, _param0.Y, num, num), -90f, 90f);
    graphicsPath.AddArc(new Rectangle(_param0.Width - num + _param0.X, _param0.Height - num + _param0.Y, num, num), 0.0f, 90f);
    graphicsPath.AddArc(new Rectangle(_param0.X, _param0.Height - num + _param0.Y, num, num), 90f, 90f);
    graphicsPath.AddLine(new Point(_param0.X, _param0.Height - num + _param0.Y), new Point(_param0.X, _param1 + _param0.Y));
    return graphicsPath;
  }

  public static GraphicsPath \u206A⁭⁮​⁫‬‫‌​⁮‭‬‮‭‫‏⁭⁮‏‭‭‏⁬⁭⁭⁬⁯‫⁬‮⁪⁭⁬⁯⁮‫‫⁪⁯‌‮(
    int _param0,
    int _param1,
    int _param2,
    int _param3,
    int _param4)
  {
    Rectangle rectangle = new Rectangle(_param0, _param1, _param2, _param3);
    GraphicsPath graphicsPath = new GraphicsPath();
    int num = _param4 * 2;
    graphicsPath.AddArc(new Rectangle(rectangle.X, rectangle.Y, num, num), -180f, 90f);
    graphicsPath.AddArc(new Rectangle(rectangle.Width - num + rectangle.X, rectangle.Y, num, num), -90f, 90f);
    graphicsPath.AddArc(new Rectangle(rectangle.Width - num + rectangle.X, rectangle.Height - num + rectangle.Y, num, num), 0.0f, 90f);
    graphicsPath.AddArc(new Rectangle(rectangle.X, rectangle.Height - num + rectangle.Y, num, num), 90f, 90f);
    graphicsPath.AddLine(new Point(rectangle.X, rectangle.Height - num + rectangle.Y), new Point(rectangle.X, _param4 + rectangle.Y));
    return graphicsPath;
  }

  public static GraphicsPath \u202C‭⁮‬‏‍⁫‮‬⁬⁫⁬​⁭‮‪‫‎‫⁫‭‮⁭⁬‫​‫⁫‍⁮⁯⁭‭⁪‭‬⁮‌‬‮(
    Rectangle _param0,
    int _param1)
  {
    GraphicsPath graphicsPath = new GraphicsPath();
    int num = _param1 * 2;
    graphicsPath.AddArc(new Rectangle(_param0.X, _param0.Y, num, num), -180f, 90f);
    graphicsPath.AddArc(new Rectangle(_param0.Width - num + _param0.X, _param0.Y, num, num), -90f, 90f);
    graphicsPath.AddLine(new Point(_param0.X + _param0.Width, _param0.Y + num), new Point(_param0.X + _param0.Width, _param0.Y + _param0.Height - 1));
    graphicsPath.AddLine(new Point(_param0.X, _param0.Height - 1 + _param0.Y), new Point(_param0.X, _param0.Y + _param1));
    return graphicsPath;
  }
}
