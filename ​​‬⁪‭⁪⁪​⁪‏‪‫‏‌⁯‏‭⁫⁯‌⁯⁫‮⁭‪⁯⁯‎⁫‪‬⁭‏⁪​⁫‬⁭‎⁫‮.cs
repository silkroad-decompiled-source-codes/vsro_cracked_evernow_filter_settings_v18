﻿// Decompiled with JetBrains decompiler
// Type: ​​‬⁪‭⁪⁪​⁪‏‪‫‏‌⁯‏‭⁫⁯‌⁯⁫‮⁭‪⁯⁯‎⁫‪‬⁭‏⁪​⁫‬⁭‎⁫‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

internal class \u200B​‬⁪‭⁪⁪​⁪‏‪‫‏‌⁯‏‭⁫⁯‌⁯⁫‮⁭‪⁯⁯‎⁫‪‬⁭‏⁪​⁫‬⁭‎⁫‮ : ContainerControl
{
  private GraphicsPath \u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;

  public \u200B​‬⁪‭⁪⁪​⁪‏‪‫‏‌⁯‏‭⁫⁯‌⁯⁫‮⁭‪⁯⁯‎⁫‪‬⁭‏⁪​⁫‬⁭‎⁫‮()
  {
    this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
    this.SetStyle(ControlStyles.UserPaint, true);
    this.BackColor = Color.Transparent;
    this.Size = new Size(187, 117);
    this.Padding = new Padding(5, 5, 5, 5);
    this.DoubleBuffered = true;
  }

  void Control.\u200E⁬⁪‎‮⁬‪⁫⁪‭‪‌⁯‫‬⁪‮​⁫⁮‫⁬⁫‪‮‬‫⁭⁯⁭‪⁬‍​‍‎‫‏⁬‎‮(EventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnResize(_param1));
    this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮ = new GraphicsPath();
    GraphicsPath graphicsPath = this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;
    graphicsPath.AddArc(0, 0, 10, 10, 180f, 90f);
    graphicsPath.AddArc(this.Width - 11, 0, 10, 10, -90f, 90f);
    graphicsPath.AddArc(this.Width - 11, this.Height - 11, 10, 10, 0.0f, 90f);
    graphicsPath.AddArc(0, this.Height - 11, 10, 10, 90f, 90f);
    graphicsPath.CloseAllFigures();
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
    Bitmap bitmap = new Bitmap(this.Width, this.Height);
    Graphics graphics = Graphics.FromImage((Image) bitmap);
    graphics.SmoothingMode = SmoothingMode.HighQuality;
    graphics.Clear(Color.Transparent);
    graphics.FillPath(Brushes.White, this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
    graphics.DrawPath(new Pen(Color.FromArgb(180, 180, 180)), this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
    graphics.Dispose();
    _param1.Graphics.DrawImage((Image) bitmap.Clone(), 0, 0);
    bitmap.Dispose();
  }
}
