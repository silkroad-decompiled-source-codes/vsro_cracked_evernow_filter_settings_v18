﻿// Decompiled with JetBrains decompiler
// Type: ‬‎‭‍⁪⁫⁬‫‮⁬‭⁬⁮‍⁮‪‪‌⁬‬⁮​⁯​‍‫‮⁬⁪‍‬‌‬⁫⁫‮⁯⁫‏⁫‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[TypeLibType(4160)]
[Guid("A6207B2E-7CDD-426A-951E-5E1CBC5AFEAD")]
[ComImport]
public interface \u202C‎‭‍⁪⁫⁬‫‮⁬‭⁬⁮‍⁮‪‪‌⁬‬⁮​⁯​‍‫‮⁬⁪‍‬‌‬⁫⁫‮⁯⁫‏⁫‮
{
  [DispId(1)]
  bool \u200E‭​‌⁪⁮‮‬‏‭‍‪‮‬⁮‏⁭‎⁯⁮‫⁮‎‪‎‎⁪‌‏‬‪‌‮‮‮‬‍‎‌‎‮ { [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(1), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(2)]
  bool \u200C⁫⁬‭‌⁫⁮‪‏​‭⁬‫⁯⁯‮‮‪‍‏‎‬‎‌‍⁭‬‬⁭‌‫⁬‌‬⁬‪​⁫‎⁮‮ { [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(2), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(3)]
  bool \u200F‪​‎⁬‮‏‍‬‮‪‪‌⁬⁫‬⁯⁬⁮⁯‭‎‭‌‏⁬⁮‭⁬‪‫⁭⁯⁪‎‭‮⁮​‭‮ { [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(4)]
  bool \u200E⁭⁪⁫‬⁭⁯‌​‫‪​‪⁬⁪‎‫⁬‪‏‮‮‮‬‏⁪‪⁭⁮⁮‌⁫‮‍⁮‪⁯‌‮⁬‮ { [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(4), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(5)]
  bool \u202A⁫⁫‍‪⁬‬‌​‭‮‮⁭‎‫‌⁭⁭‮⁭‎⁪⁯‎‎‭⁮‭‪​​⁬⁭⁮‎⁬‍⁫‍⁪‮ { [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(6)]
  bool \u206F⁮‏⁪‎⁫⁭⁬⁯‎‎‭‪‬‭⁮‫‎‭‎‪⁪‭‮⁬‬​‭⁪‭⁫⁬‬⁬⁫‌⁮⁭‬⁫‮ { [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(6), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(7)]
  bool \u206F‫‍‭⁬‮‭⁫‌‬‏‎‬‏‭⁮‏‫‮‎​⁮⁯‭⁬‪⁯⁭⁯⁭‭⁮‌​‏⁬‍‬‎‪‮ { [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(7), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(8)]
  bool \u200E⁬⁯‫‬‌⁫‪⁫‭‫⁪‌‌‭⁭⁭‍‫‭‭⁭‎‏‫‭⁮‌‫⁪‮⁪‬‬‭‌⁭‌⁮‮ { [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(8), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(9)]
  bool \u206B⁭⁮‎‏‬⁯⁯⁬‫‬⁪⁯‮⁫⁭⁮‪‌‫⁮⁬⁭‏⁯‪⁯⁭‬‏‬‭‮⁬‪‪‭‬⁮‬‮ { [DispId(9), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(9), MethodImpl(MethodImplOptions.InternalCall)] set; }

  [DispId(10)]
  bool \u206F‍‮⁪​⁮‫‍‎⁮‭‭‏⁪‮⁫‌‫‏‬‍‪‪⁯⁭‪‪‎‎⁬‎‏⁭⁬‍‬⁪⁯⁯⁪‮ { [DispId(10), MethodImpl(MethodImplOptions.InternalCall)] get; [DispId(10), MethodImpl(MethodImplOptions.InternalCall)] set; }
}
