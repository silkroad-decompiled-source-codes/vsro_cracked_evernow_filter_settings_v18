﻿// Decompiled with JetBrains decompiler
// Type: ‎‭​‎‭​‪‍⁫‎⁫‌‌‌‏‮‭‫⁭‏⁭⁬‫‬⁬‮⁮‪⁪⁫⁯‪‍​‪⁫‪⁫‮‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

internal class \u200E‭​‎‭​‪‍⁫‎⁫‌‌‌‏‮‭‫⁭‏⁭⁬‫‬⁬‮⁮‪⁪⁫⁯‪‍​‪⁫‪⁫‮‮ : Control
{
  private StringAlignment \u206B​‮‬⁮‮‏‏⁮⁬⁮‍⁮‏‬‫​‍‎‎⁯‌‭‎‫⁮‬‭‭⁪‫‫⁫‍⁮⁬⁬‫⁪‬‮ = StringAlignment.Center;
  private Color \u200C‪⁯⁮⁪‫​‫‮⁪‎‏⁮⁮‭⁭​‪⁪⁭‪‏‏⁯⁫⁪​⁯⁮‪‭⁬‮‬⁬⁮‮⁫⁮‮ = Color.FromArgb(150, 150, 150);
  private ContentAlignment \u206A⁯‏‏‏‏‫⁯⁮⁫⁫‍‫⁯⁪⁭⁭​‪⁬‌‌⁪‪⁮‎‭‪⁪‫⁫⁭​⁯⁭​⁭⁯‍⁬‮ = ContentAlignment.MiddleLeft;
  private int \u206D‭⁮⁭‭⁭‎⁯⁪‫‬‏‬‭‮⁪‎⁪‎‌‮⁭⁯‎‮‏‪‪‪‌⁮⁬⁮⁬‪‭⁫⁬⁭‏‮;
  private GraphicsPath \u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;
  private LinearGradientBrush \u206B‎⁮‪‌⁫‭‫⁯⁪‫‍‭‮​‬‍⁮⁬‭‪‍‫⁭‫⁭‏⁮‫‮‭‬‭‫‮‭⁮‫‬‎‮;
  private LinearGradientBrush \u202D‬⁮‏‬‪‮‪⁯⁮​‌⁯‏⁯⁫⁮‫‮‍​‮‏⁬⁮⁭‍‏​‌‌⁮⁬‭⁮‎‏‪⁫‎‮;
  private LinearGradientBrush \u202D⁭‬⁫‏‍⁮⁭‌‬‫⁬⁮‌‏‎‏⁮⁫⁭‏​‍‮‏⁬⁫⁪‍‫‫⁯‍​⁫‍‌‮⁫⁫‮;
  private Rectangle \u202D‭​⁭⁮‬⁬⁫⁪⁬‎‎​⁪‍‍‍‌⁯‬⁪‮⁭‏⁯‪⁮‪‫⁯‫‫‏‬‬‬⁫‏‌‏‮;
  private Pen \u202E⁮‭⁫‍⁬⁫‮⁫‎‬​‭‬‬⁮​‎​⁯⁮⁬‌‌⁬⁮⁮⁫‏‮⁯‮⁪‌‎‪‪⁫‬‫‮;
  private Pen \u200D⁪‪⁪‫‏‬⁮⁯‭‏‬‪⁪⁫‏‮​⁮‏⁪⁪‮⁪‮‬⁬⁭‍‪⁯⁬‍⁮‪⁬⁪‏‭⁮‮;
  private Image \u200B‭⁫‪⁮​‏‌‬‪⁫‎⁫⁪⁫⁯‬⁫⁮‭⁬​‫⁫⁫⁪‏⁪⁫⁯‬‎‭​​‌‬⁯‌‪‮;
  private Size \u200B‌​⁮‪⁫⁫‎⁮⁭⁫‭‫‌‬⁭‪⁮‪‍‬‌⁯⁯⁫‍‮⁭‫‎​⁫​‫⁮⁮‍‪⁯‏‮;

  private static PointF \u206C‮‎‌‭‪⁮⁬‫‍⁬⁭‭⁫⁯‍⁯⁯‫⁯⁭‏‌‏⁫‬​‍⁫⁬​‫‭⁯⁯‭‏‫⁪⁪‮(
    StringFormat _param0,
    SizeF _param1,
    SizeF _param2)
  {
    PointF pointF = new PointF();
    switch (_param0.Alignment)
    {
      case StringAlignment.Near:
        pointF.X = 2f;
        break;
      case StringAlignment.Center:
        pointF.X = Convert.ToSingle((float) (((double) _param1.Width - (double) _param2.Width) / 2.0));
        break;
      case StringAlignment.Far:
        pointF.X = (float) ((double) _param1.Width - (double) _param2.Width - 2.0);
        break;
    }
    switch (_param0.LineAlignment)
    {
      case StringAlignment.Near:
        pointF.Y = 2f;
        break;
      case StringAlignment.Center:
        pointF.Y = Convert.ToSingle((float) (((double) _param1.Height - (double) _param2.Height) / 2.0));
        break;
      case StringAlignment.Far:
        pointF.Y = (float) ((double) _param1.Height - (double) _param2.Height - 2.0);
        break;
    }
    return pointF;
  }

  private StringFormat \u200D‍‎⁯​‭⁭‏‫‪‌‎‬⁬⁬‪‎⁯⁯‪⁮‫‪‭‭⁯‌⁮⁪‬‌‪‏‪‭‭‌‍‮‌‮(
    ContentAlignment _param1)
  {
    StringFormat stringFormat = new StringFormat();
    switch (_param1)
    {
      case ContentAlignment.TopLeft:
        stringFormat.LineAlignment = StringAlignment.Near;
        stringFormat.Alignment = StringAlignment.Near;
        break;
      case ContentAlignment.TopCenter:
        stringFormat.LineAlignment = StringAlignment.Near;
        stringFormat.Alignment = StringAlignment.Center;
        break;
      case ContentAlignment.TopRight:
        stringFormat.LineAlignment = StringAlignment.Near;
        stringFormat.Alignment = StringAlignment.Far;
        break;
      case ContentAlignment.MiddleLeft:
        stringFormat.LineAlignment = StringAlignment.Center;
        stringFormat.Alignment = StringAlignment.Near;
        break;
      case ContentAlignment.MiddleCenter:
        stringFormat.LineAlignment = StringAlignment.Center;
        stringFormat.Alignment = StringAlignment.Center;
        break;
      case ContentAlignment.MiddleRight:
        stringFormat.LineAlignment = StringAlignment.Center;
        stringFormat.Alignment = StringAlignment.Far;
        break;
      case ContentAlignment.BottomLeft:
        stringFormat.LineAlignment = StringAlignment.Far;
        stringFormat.Alignment = StringAlignment.Near;
        break;
      case ContentAlignment.BottomCenter:
        stringFormat.LineAlignment = StringAlignment.Far;
        stringFormat.Alignment = StringAlignment.Center;
        break;
      case ContentAlignment.BottomRight:
        stringFormat.LineAlignment = StringAlignment.Far;
        stringFormat.Alignment = StringAlignment.Far;
        break;
    }
    return stringFormat;
  }

  public Image \u202C⁪‪⁪⁪⁬⁪‬‭⁬⁯⁪⁪⁭‮⁪‍⁪‏‏‭‌⁪⁯⁭​‌⁪‪‪‍⁮‎​⁫​⁪‫⁯‭‮
  {
    get
    {
      return this.\u200B‭⁫‪⁮​‏‌‬‪⁫‎⁫⁪⁫⁯‬⁫⁮‭⁬​‫⁫⁫⁪‏⁪⁫⁯‬‎‭​​‌‬⁯‌‪‮;
    }
    set
    {
      this.\u200B‌​⁮‪⁫⁫‎⁮⁭⁫‭‫‌‬⁭‪⁮‪‍‬‌⁯⁯⁫‍‮⁭‫‎​⁫​‫⁮⁮‍‪⁯‏‮ = value != null ? value.Size : Size.Empty;
      this.\u200B‭⁫‪⁮​‏‌‬‪⁫‎⁫⁪⁫⁯‬⁫⁮‭⁬​‫⁫⁫⁪‏⁪⁫⁯‬‎‭​​‌‬⁯‌‪‮ = value;
      this.Invalidate();
    }
  }

  protected Size \u202C⁮⁪⁮‮‫‬⁭⁯⁭‫‎‪‬‪⁯​⁮‍⁫‎‪‫‏‌⁪⁫‪‪​‭‍‎‬‫⁬‎⁫‌‮
  {
    get
    {
      return this.\u200B‌​⁮‪⁫⁫‎⁮⁭⁫‭‫‌‬⁭‪⁮‪‍‬‌⁯⁯⁫‍‮⁭‫‎​⁫​‫⁮⁮‍‪⁯‏‮;
    }
  }

  public ContentAlignment \u206B‍‌‏‎​⁬⁯‍‎‎⁭⁪‮‎⁮⁬‪‪​⁭‌‬⁫⁭‌⁮‫‏⁮⁭⁬‍⁯⁭‪⁭‍‬⁮‮
  {
    get
    {
      return this.\u206A⁯‏‏‏‏‫⁯⁮⁫⁫‍‫⁯⁪⁭⁭​‪⁬‌‌⁪‪⁮‎‭‪⁪‫⁫⁭​⁯⁭​⁭⁯‍⁬‮;
    }
    set
    {
      this.\u206A⁯‏‏‏‏‫⁯⁮⁫⁫‍‫⁯⁪⁭⁭​‪⁬‌‌⁪‪⁮‎‭‪⁪‫⁫⁭​⁯⁭​⁭⁯‍⁬‮ = value;
      this.Invalidate();
    }
  }

  public StringAlignment \u200B⁪‌‪‮⁪⁪‍‬‎⁭‭‍​⁯‌‮‍‭‎‭⁪⁭⁬⁪‎⁪⁬‌⁬⁭‮‏‪‪‪‎⁬⁮‫‮
  {
    get
    {
      return this.\u206B​‮‬⁮‮‏‏⁮⁬⁮‍⁮‏‬‫​‍‎‎⁯‌‭‎‫⁮‬‭‭⁪‫‫⁫‍⁮⁬⁬‫⁪‬‮;
    }
    set
    {
      this.\u206B​‮‬⁮‮‏‏⁮⁬⁮‍⁮‏‬‫​‍‎‎⁯‌‭‎‫⁮‬‭‭⁪‫‫⁫‍⁮⁬⁬‫⁪‬‮ = value;
      this.Invalidate();
    }
  }

  public virtual Color \u206F⁬⁯‭‎⁮‮‮‪‭⁯‎‬⁭‮‫‌‭‭‪⁪‏‬⁪⁬⁮​‎‎‮‍⁫‫⁭‎‍‎‬‍⁭‮
  {
    get
    {
      return this.\u200C‪⁯⁮⁪‫​‫‮⁪‎‏⁮⁮‭⁭​‪⁪⁭‪‏‏⁯⁫⁪​⁯⁮‪‭⁬‮‬⁬⁮‮⁫⁮‮;
    }
    set
    {
      this.\u200C‪⁯⁮⁪‫​‫‮⁪‎‏⁮⁮‭⁭​‪⁪⁭‪‏‏⁯⁫⁪​⁯⁮‪‭⁬‮‬⁬⁮‮⁫⁮‮ = value;
      this.Invalidate();
    }
  }

  void Control.\u200F⁫‪‫‎⁫⁭‏‏‭​‭‫‬‌⁪⁫⁬⁮⁪⁭‫‮⁬⁯‏⁪​​‎‪‫⁪​‪‮‏⁫‪‮(MouseEventArgs _param1)
  {
    this.\u206D‭⁮⁭‭⁭‎⁯⁪‫‬‏‬‭‮⁪‎⁪‎‌‮⁭⁯‎‮‏‪‪‪‌⁮⁬⁮⁬‪‭⁫⁬⁭‏‮ = 0;
    this.Invalidate();
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnMouseUp(_param1));
  }

  void Control.\u206F‮⁪⁫‍​‫⁪‭‪‭‍​‭‏⁯⁬‪⁪‮⁭‏⁮‮​‫​‮⁯⁮‮‭‏‏‮⁪‏‫⁪⁮‮(MouseEventArgs _param1)
  {
    this.\u206D‭⁮⁭‭⁭‎⁯⁪‫‬‏‬‭‮⁪‎⁪‎‌‮⁭⁯‎‮‏‪‪‪‌⁮⁬⁮⁬‪‭⁫⁬⁭‏‮ = 1;
    this.Invalidate();
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnMouseDown(_param1));
  }

  void Control.\u200B‫‍⁯‌‬⁫⁪‮⁭‎⁭‏‬‎‏‏⁪⁮⁬⁭⁮‪‌⁬‎⁯⁫​‪‭‌⁬⁪‌⁭‌⁭⁫‌‮(EventArgs _param1)
  {
    this.\u206D‭⁮⁭‭⁭‎⁯⁪‫‬‏‬‭‮⁪‎⁪‎‌‮⁭⁯‎‮‏‪‪‪‌⁮⁬⁮⁬‪‭⁫⁬⁭‏‮ = 0;
    this.Invalidate();
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnMouseLeave(_param1));
  }

  void Control.\u202C‮⁭⁫‎‌⁬⁭‮​‍‍⁬‏‭⁫‫⁮⁪‮‭‌‌‏⁮‏‫‪‪⁫‬⁯‍⁫⁭‍​‍⁮⁭‮(EventArgs _param1)
  {
    this.Invalidate();
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnTextChanged(_param1));
  }

  public \u200E‭​‎‭​‪‍⁫‎⁫‌‌‌‏‮‭‫⁭‏⁭⁬‫‬⁬‮⁮‪⁪⁫⁯‪‍​‪⁫‪⁫‮‮()
  {
    this.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
    this.BackColor = Color.Transparent;
    this.DoubleBuffered = true;
    this.Font = new Font("Segoe UI", 12f);
    ((Control) this).ForeColor = Color.FromArgb(150, 150, 150);
    this.Size = new Size(166, 40);
    this.\u206B​‮‬⁮‮‏‏⁮⁬⁮‍⁮‏‬‫​‍‎‎⁯‌‭‎‫⁮‬‭‭⁪‫‫⁫‍⁮⁬⁬‫⁪‬‮ = StringAlignment.Center;
    this.\u202E⁮‭⁫‍⁬⁫‮⁫‎‬​‭‬‬⁮​‎​⁯⁮⁬‌‌⁬⁮⁮⁫‏‮⁯‮⁪‌‎‪‪⁫‬‫‮ = new Pen(Color.FromArgb(190, 190, 190));
  }

  void Control.\u200E⁬⁪‎‮⁬‪⁫⁪‭‪‌⁯‫‬⁪‮​⁫⁮‫⁬⁫‪‮‬‫⁭⁯⁭‪⁬‍​‍‎‫‏⁬‎‮(EventArgs _param1)
  {
    if ((this.Width > 0 ? (this.Height > 0 ? 1 : 0) : 0) != 0)
    {
      this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮ = new GraphicsPath();
      this.\u202D‭​⁭⁮‬⁬⁫⁪⁬‎‎​⁪‍‍‍‌⁯‬⁪‮⁭‏⁯‪⁮‪‫⁯‫‫‏‬‬‬⁫‏‌‏‮ = new Rectangle(0, 0, this.Width, this.Height);
      this.\u206B‎⁮‪‌⁫‭‫⁯⁪‫‍‭‮​‬‍⁮⁬‭‪‍‫⁭‫⁭‏⁮‫‮‭‬‭‫‮‭⁮‫‬‎‮ = new LinearGradientBrush(new Rectangle(0, 0, this.Width, this.Height), Color.FromArgb(251, 251, 251), Color.FromArgb(225, 225, 225), 90f);
      this.\u202D‬⁮‏‬‪‮‪⁯⁮​‌⁯‏⁯⁫⁮‫‮‍​‮‏⁬⁮⁭‍‏​‌‌⁮⁬‭⁮‎‏‪⁫‎‮ = new LinearGradientBrush(new Rectangle(0, 0, this.Width, this.Height), Color.FromArgb(235, 235, 235), Color.FromArgb(223, 223, 223), 90f);
      this.\u202D⁭‬⁫‏‍⁮⁭‌‬‫⁬⁮‌‏‎‏⁮⁫⁭‏​‍‮‏⁬⁫⁪‍‫‫⁯‍​⁫‍‌‮⁫⁫‮ = new LinearGradientBrush(new Rectangle(0, 0, this.Width, this.Height), Color.FromArgb(167, 167, 167), Color.FromArgb(167, 167, 167), 90f);
      this.\u200D⁪‪⁪‫‏‬⁮⁯‭‏‬‪⁪⁫‏‮​⁮‏⁪⁪‮⁪‮‬⁬⁭‍‪⁯⁬‍⁮‪⁬⁪‏‭⁮‮ = new Pen((Brush) this.\u202D⁭‬⁫‏‍⁮⁭‌‬‫⁬⁮‌‏‎‏⁮⁫⁭‏​‍‮‏⁬⁫⁪‍‫‫⁯‍​⁫‍‌‮⁫⁫‮);
    }
    GraphicsPath graphicsPath = this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮;
    graphicsPath.AddArc(0, 0, 10, 10, 180f, 90f);
    graphicsPath.AddArc(this.Width - 11, 0, 10, 10, -90f, 90f);
    graphicsPath.AddArc(this.Width - 11, this.Height - 11, 10, 10, 0.0f, 90f);
    graphicsPath.AddArc(0, this.Height - 11, 10, 10, 90f, 90f);
    graphicsPath.CloseAllFigures();
    this.Invalidate();
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnResize(_param1));
  }

  void Control.\u202A⁪‌‮⁮‬⁬‫‬‎‏‎‬‎‭⁫⁫‍‎‮‪‬‌‬⁯‍⁬‮‪⁯‫‬‪⁭⁫‌‫‭‮‪‮(PaintEventArgs _param1)
  {
    Graphics graphics1 = _param1.Graphics;
    graphics1.SmoothingMode = SmoothingMode.HighQuality;
    PointF pointF = \u200E‭​‎‭​‪‍⁫‎⁫‌‌‌‏‮‭‫⁭‏⁭⁬‫‬⁬‮⁮‪⁪⁫⁯‪‍​‪⁫‪⁫‮‮.\u206C‮‎‌‭‪⁮⁬‫‍⁬⁭‭⁫⁯‍⁯⁯‫⁯⁭‏‌‏⁫‬​‍⁫⁬​‫‭⁯⁯‭‏‫⁪⁪‮(this.\u200D‍‎⁯​‭⁭‏‫‪‌‎‬⁬⁬‪‎⁯⁯‪⁮‫‪‭‭⁯‌⁮⁪‬‌‪‏‪‭‭‌‍‮‌‮(this.\u206B‍‌‏‎​⁬⁯‍‎‎⁭⁪‮‎⁮⁬‪‪​⁭‌‬⁫⁭‌⁮‫‏⁮⁭⁬‍⁯⁭‪⁭‍‬⁮‮), (SizeF) this.Size, (SizeF) this.\u202C⁮⁪⁮‮‫‬⁭⁯⁭‫‎‪‬‪⁯​⁮‍⁫‎‪‫‏‌⁪⁫‪‪​‭‍‎‬‫⁬‎⁫‌‮);
    switch (this.\u206D‭⁮⁭‭⁭‎⁯⁪‫‬‏‬‭‮⁪‎⁪‎‌‮⁭⁯‎‮‏‪‪‪‌⁮⁬⁮⁬‪‭⁫⁬⁭‏‮)
    {
      case 0:
        graphics1.FillPath((Brush) this.\u206B‎⁮‪‌⁫‭‫⁯⁪‫‍‭‮​‬‍⁮⁬‭‪‍‫⁭‫⁭‏⁮‫‮‭‬‭‫‮‭⁮‫‬‎‮, this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
        graphics1.DrawPath(this.\u202E⁮‭⁫‍⁬⁫‮⁫‎‬​‭‬‬⁮​‎​⁯⁮⁬‌‌⁬⁮⁮⁫‏‮⁯‮⁪‌‎‪‪⁫‬‫‮, this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
        if (this.\u202C⁪‪⁪⁪⁬⁪‬‭⁬⁯⁪⁪⁭‮⁪‍⁪‏‏‭‌⁪⁯⁭​‌⁪‪‪‍⁮‎​⁫​⁪‫⁯‭‮ == null)
        {
          graphics1.DrawString(this.Text, this.Font, (Brush) new SolidBrush(((Control) this).ForeColor), (RectangleF) this.\u202D‭​⁭⁮‬⁬⁫⁪⁬‎‎​⁪‍‍‍‌⁯‬⁪‮⁭‏⁯‪⁮‪‫⁯‫‫‏‬‬‬⁫‏‌‏‮, new StringFormat()
          {
            Alignment = this.\u206B​‮‬⁮‮‏‏⁮⁬⁮‍⁮‏‬‫​‍‎‎⁯‌‭‎‫⁮‬‭‭⁪‫‫⁫‍⁮⁬⁬‫⁪‬‮,
            LineAlignment = StringAlignment.Center
          });
          break;
        }
        Graphics graphics2 = graphics1;
        Image image1 = this.\u200B‭⁫‪⁮​‏‌‬‪⁫‎⁫⁪⁫⁯‬⁫⁮‭⁬​‫⁫⁫⁪‏⁪⁫⁯‬‎‭​​‌‬⁯‌‪‮;
        double x1 = (double) pointF.X;
        double y1 = (double) pointF.Y;
        Size size1 = this.\u202C⁮⁪⁮‮‫‬⁭⁯⁭‫‎‪‬‪⁯​⁮‍⁫‎‪‫‏‌⁪⁫‪‪​‭‍‎‬‫⁬‎⁫‌‮;
        double width1 = (double) size1.Width;
        size1 = this.\u202C⁮⁪⁮‮‫‬⁭⁯⁭‫‎‪‬‪⁯​⁮‍⁫‎‪‫‏‌⁪⁫‪‪​‭‍‎‬‫⁬‎⁫‌‮;
        double height1 = (double) size1.Height;
        graphics2.DrawImage(image1, (float) x1, (float) y1, (float) width1, (float) height1);
        graphics1.DrawString(this.Text, this.Font, (Brush) new SolidBrush(((Control) this).ForeColor), (RectangleF) this.\u202D‭​⁭⁮‬⁬⁫⁪⁬‎‎​⁪‍‍‍‌⁯‬⁪‮⁭‏⁯‪⁮‪‫⁯‫‫‏‬‬‬⁫‏‌‏‮, new StringFormat()
        {
          Alignment = this.\u206B​‮‬⁮‮‏‏⁮⁬⁮‍⁮‏‬‫​‍‎‎⁯‌‭‎‫⁮‬‭‭⁪‫‫⁫‍⁮⁬⁬‫⁪‬‮,
          LineAlignment = StringAlignment.Center
        });
        break;
      case 1:
        graphics1.FillPath((Brush) this.\u202D‬⁮‏‬‪‮‪⁯⁮​‌⁯‏⁯⁫⁮‫‮‍​‮‏⁬⁮⁭‍‏​‌‌⁮⁬‭⁮‎‏‪⁫‎‮, this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
        graphics1.DrawPath(this.\u200D⁪‪⁪‫‏‬⁮⁯‭‏‬‪⁪⁫‏‮​⁮‏⁪⁪‮⁪‮‬⁬⁭‍‪⁯⁬‍⁮‪⁬⁪‏‭⁮‮, this.\u206A‌⁯⁯‍‫‪⁭‎‭‫⁮‍‪​‏​⁮⁭⁮⁭‭‎‌‎‏⁫‪⁪⁮‏‎‏‮⁪‮⁭‬​‎‮);
        if (this.\u202C⁪‪⁪⁪⁬⁪‬‭⁬⁯⁪⁪⁭‮⁪‍⁪‏‏‭‌⁪⁯⁭​‌⁪‪‪‍⁮‎​⁫​⁪‫⁯‭‮ == null)
        {
          graphics1.DrawString(this.Text, this.Font, (Brush) new SolidBrush(((Control) this).ForeColor), (RectangleF) this.\u202D‭​⁭⁮‬⁬⁫⁪⁬‎‎​⁪‍‍‍‌⁯‬⁪‮⁭‏⁯‪⁮‪‫⁯‫‫‏‬‬‬⁫‏‌‏‮, new StringFormat()
          {
            Alignment = this.\u206B​‮‬⁮‮‏‏⁮⁬⁮‍⁮‏‬‫​‍‎‎⁯‌‭‎‫⁮‬‭‭⁪‫‫⁫‍⁮⁬⁬‫⁪‬‮,
            LineAlignment = StringAlignment.Center
          });
          break;
        }
        Graphics graphics3 = graphics1;
        Image image2 = this.\u200B‭⁫‪⁮​‏‌‬‪⁫‎⁫⁪⁫⁯‬⁫⁮‭⁬​‫⁫⁫⁪‏⁪⁫⁯‬‎‭​​‌‬⁯‌‪‮;
        double x2 = (double) pointF.X;
        double y2 = (double) pointF.Y;
        Size size2 = this.\u202C⁮⁪⁮‮‫‬⁭⁯⁭‫‎‪‬‪⁯​⁮‍⁫‎‪‫‏‌⁪⁫‪‪​‭‍‎‬‫⁬‎⁫‌‮;
        double width2 = (double) size2.Width;
        size2 = this.\u202C⁮⁪⁮‮‫‬⁭⁯⁭‫‎‪‬‪⁯​⁮‍⁫‎‪‫‏‌⁪⁫‪‪​‭‍‎‬‫⁬‎⁫‌‮;
        double height2 = (double) size2.Height;
        graphics3.DrawImage(image2, (float) x2, (float) y2, (float) width2, (float) height2);
        graphics1.DrawString(this.Text, this.Font, (Brush) new SolidBrush(((Control) this).ForeColor), (RectangleF) this.\u202D‭​⁭⁮‬⁬⁫⁪⁬‎‎​⁪‍‍‍‌⁯‬⁪‮⁭‏⁯‪⁮‪‫⁯‫‫‏‬‬‬⁫‏‌‏‮, new StringFormat()
        {
          Alignment = this.\u206B​‮‬⁮‮‏‏⁮⁬⁮‍⁮‏‬‫​‍‎‎⁯‌‭‎‫⁮‬‭‭⁪‫‫⁫‍⁮⁬⁬‫⁪‬‮,
          LineAlignment = StringAlignment.Center
        });
        break;
    }
    // ISSUE: explicit non-virtual call
    __nonvirtual (((Control) this).OnPaint(_param1));
  }
}
