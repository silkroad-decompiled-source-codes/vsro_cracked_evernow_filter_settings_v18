﻿// Decompiled with JetBrains decompiler
// Type: ⁭‏‮⁬‍‭​⁪‎⁪⁬‌‬⁯⁯​⁯⁬‏‏‎⁮⁪⁪⁬⁯‏‌‎‮‬‮⁯‬⁯‎‌‍⁫⁫‮
// Assembly: EverNow [Filter Settings], Version=18.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D82354E4-D9C0-4D92-A268-FEE038D9DB51
// Assembly location: C:\Users\Syinea\Downloads\EverNow_Filter__V18__Premium_\EverNow [Filter Settings].exe

using System;
using System.Drawing;
using System.Drawing.Drawing2D;

public class \u206D‏‮⁬‍‭​⁪‎⁪⁬‌‬⁯⁯​⁯⁬‏‏‎⁮⁪⁪⁬⁯‏‌‎‮‬‮⁯‬⁯‎‌‍⁫⁫‮
{
  public static void \u202D‌‌⁯‎⁪⁭⁭‭‌‭‏​‏‮‏‬‫⁮⁬⁮⁬‎​‫‌‬⁯‍⁪‭‌‭‭⁯‌⁮‫‎‍‮(
    Graphics _param0,
    \u206C‏⁭‮⁭⁯⁯‫⁫‫‏‌‬⁬‬​⁯⁪⁮‪⁯⁯⁫‮⁬‫⁬‪‌‏⁮‌⁫⁫⁭⁭‌⁭⁫⁬‮ _param1,
    Rectangle _param2)
  {
    Rectangle rectangle1 = new Rectangle();
    Rectangle rect1 = new Rectangle();
    Rectangle rectangle2 = new Rectangle(_param2.X + 1, _param2.Y + 1, _param2.Width - 1, _param2.Height - 1);
    Rectangle rect2 = rectangle2;
    rect2.Height -= Convert.ToInt32(rect2.Height / 2);
    rect1 = new Rectangle(rect2.X, rect2.Bottom, rect2.Width, rectangle2.Height - rect2.Height);
    using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(rect2, _param1.\u202B⁮⁬⁫‮⁮⁪‍‎‭‫‌‎‍‎⁭‬‭‭‪‭​⁪‭​⁪​⁭‎⁪‮⁮‮‮​‭⁪⁪‏‬‮, _param1.\u200B⁮‮‫⁮⁭‭‮‫⁯⁬‭‪⁫​⁫‫⁭⁭‌⁭‬‮⁫⁪⁮​⁭⁯‏‬‫⁭⁫⁮⁯‏⁬⁮‮‮, LinearGradientMode.Vertical))
      _param0.FillRectangle((Brush) linearGradientBrush, rect2);
    using (SolidBrush solidBrush = new SolidBrush(_param1.\u206B‮‬‌⁬⁭‌​‎⁬⁪‮‪‍⁪⁪‮‪⁬​‌‏⁮​⁭‪‍‪​⁮‭⁮⁯‫‬‌‏‪‬‪‮))
      _param0.FillRectangle((Brush) solidBrush, rect1);
    using (Pen pen = new Pen(_param1.\u202C⁮‪‪‮‮⁯⁫‎‪​​‫⁫⁫‮⁯⁬‎‮‏‫⁬‬‎‮​‮‌‬‎‏⁪‪⁫‬⁯‬⁯‮))
      \u206D‏‮⁬‍‭​⁪‎⁪⁬‌‬⁯⁯​⁯⁬‏‏‎⁮⁪⁪⁬⁯‏‌‎‮‬‮⁯‬⁯‎‌‍⁫⁫‮.\u200C‪⁯‪⁬⁯⁬⁬‫‫⁯⁫‎‍⁪‏‎‌‏‌‍‍‭​‫‫‍‎⁬‏‭‪‫‌⁭‭‏⁯‏‭‮(_param0, pen, Convert.ToSingle(_param2.X), Convert.ToSingle(_param2.Y), Convert.ToSingle(_param2.Width), Convert.ToSingle(_param2.Height), 2f);
  }

  public static void \u200C‪⁯‪⁬⁯⁬⁬‫‫⁯⁫‎‍⁪‏‎‌‏‌‍‍‭​‫‫‍‎⁬‏‭‪‫‌⁭‭‏⁯‏‭‮(
    Graphics _param0,
    Pen _param1,
    float _param2,
    float _param3,
    float _param4,
    float _param5,
    float _param6)
  {
    using (GraphicsPath path = new GraphicsPath())
    {
      path.AddLine(_param2 + _param6, _param3, (float) ((double) _param2 + (double) _param4 - (double) _param6 * 2.0), _param3);
      path.AddArc((float) ((double) _param2 + (double) _param4 - (double) _param6 * 2.0), _param3, _param6 * 2f, _param6 * 2f, 270f, 90f);
      path.AddLine(_param2 + _param4, _param3 + _param6, _param2 + _param4, (float) ((double) _param3 + (double) _param5 - (double) _param6 * 2.0));
      path.AddArc((float) ((double) _param2 + (double) _param4 - (double) _param6 * 2.0), (float) ((double) _param3 + (double) _param5 - (double) _param6 * 2.0), _param6 * 2f, _param6 * 2f, 0.0f, 90f);
      path.AddLine((float) ((double) _param2 + (double) _param4 - (double) _param6 * 2.0), _param3 + _param5, _param2 + _param6, _param3 + _param5);
      path.AddArc(_param2, (float) ((double) _param3 + (double) _param5 - (double) _param6 * 2.0), _param6 * 2f, _param6 * 2f, 90f, 90f);
      path.AddLine(_param2, (float) ((double) _param3 + (double) _param5 - (double) _param6 * 2.0), _param2, _param3 + _param6);
      path.AddArc(_param2, _param3, _param6 * 2f, _param6 * 2f, 180f, 90f);
      path.CloseFigure();
      _param0.SmoothingMode = SmoothingMode.AntiAlias;
      _param0.DrawPath(_param1, path);
      _param0.SmoothingMode = SmoothingMode.Default;
    }
  }
}
